From mathcomp Require Import all_ssreflect finmap.

From Coq Require Import Classical.

From Undecidability.FOL.TRAKHTENBROT Require Import enumerable.

From QRC1 Require Import Language QRC1.
From QRC1 Require Import KripkeSemantics KripkeSoundness KripkeCompleteness.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Section Enumerability.

Open Scope qsp_scope.

Variable sig : signature.
Notation formula := (formula sig).

Definition QRC1Proof_pair (AB : formula * formula) : Prop :=
  |- AB.1 ~> AB.2.

Lemma QRC1Proof_enum : rec_enum_t QRC1Proof_pair.
Proof.
  exists (fun n AB => bQRC1Proof_bool n AB.1 AB.2) => -[A B].
  rewrite /QRC1Proof_pair /= QRC1Proof_bQRC1Proof.
  by split=> -[n /bQRC1ProofP ApnB]; exists n.
Qed.

Open Scope fset.

Definition enumerate_cmodels (n : nat) (AB : formula * formula) : bool :=
  let P := PP AB.1 AB.2 in
  let N := NN AB.1 AB.2 in
  if @choice.unpickle [countType of {fset cWType P N} * nat] n
       is Some (cworld, n') then
    if choice.unpickle n' is Some w then
      let uM := model_unlift (cmodel cworld) in
      satb (M := uM) (cg w) AB.1 && ~~ satb (M := uM) (cg w) AB.2
    else false
  else false.

Lemma nQRC1Proof_enum :
  rec_enum_t (fun (AB : formula * formula) => ~ QRC1Proof_pair AB).
Proof.
  exists enumerate_cmodels => -[A B]; rewrite /QRC1Proof_pair /=; split.
    move=> /explicit_completeness [ws [w [/satP satA /satP nsatB]]].
    exists (choice.pickle (ws, (choice.pickle w))).
    by rewrite /enumerate_cmodels !choice.pickleK /= satA.
  move=> [n].
  rewrite /enumerate_cmodels; case: (choice.unpickle n) => //= -[cworld n'].
  case: (choice.unpickle n') => //= w /andP [/satP satA /satP nsatB].
  move=> /soundness sound.
  apply: nsatB.
  by apply: (sound _ _ (model_unlift (cmodel cworld))).
Qed.

Theorem QRC1Proof_dec (A B : formula) :
  { |- A ~> B} + {~ |- A ~> B}.
Proof.
  rewrite -[A]/((A, B).1) -[X in |- _ ~> X]/((A, B).2).
  apply: (bi_rec_enum_t_dec QRC1Proof_enum nQRC1Proof_enum).
  exact: classic.
Qed.

End Enumerability.
