(*| .. coq:: none |*)
Set Warnings "-notation-overridden, -ambiguous-paths".

(*|
====================
QRC1 in Coq: summary
====================

This project is a formalization of the QRC1 logic defined in [1] and refined in
[2]. This file lists the main definitions and results already formalized,
linking them with the names and numbering of [2], with a side note on some
results from [1].

[1] A. de Almeida Borges and J. J. Joosten (2020).
Quantified Reflection Calculus with one modality.
Advances in Modal Logic 13: 13-32.
`arXiv:2003.13651 [math.LO] <https://arxiv.org/pdf/2003.13651.pdf>`_

[2] A. de Almeida Borges and J. J. Joosten (2022).
An escape from Vardanyan’s Theorem.
Journal of Symbolic Logic.
`arXiv:2102.13091 [math.LO] <https://arxiv.org/pdf/2102.13091.pdf>`_

See the banner at the top for instructions on how to navigate this file.
|*)

From mathcomp Require Import all_ssreflect finmap.
Unset Printing Implicit Defensive.

(*|
Language
========
|*)

From QRC1 Require Import Language.

(*| Variables are natural numbers. |*)
Print VarName.

(*| A signature declares finitely many constants and predicate symbols. |*)
Print signature.

(*| A term is either a variable or a constant. |*)
Print term.

(*|
A formula is either T, a predicate symbol applied to a tuple of terms, a
conjunction of two other formulas, a diamond, or a universal quantifier.
|*)
Print formula.

(*|
Formulas live within the qsp_scope (for quantified and strictly positive) and we
provide the following notations.
|*)
Open Scope qsp_scope.
Locate "A /\ B".
Locate "<> A".

(*|
The free variables and constants of a formula are defined as usual, as is the
definition of closed formula. We use the finmap library to represent finite
sets.
|*)
Open Scope fset.
Print fv.
Print constants.
Print closed.

(*|
The substitution of a term for a term is defined without regard for possible
captures of the new term by existing quantifications.
|*)
Print sub. Locate "A `[ t1 <- t2 ]".
(*|
We then define the notion of a term being free for a variable or more generally
a term in a formula, which protects against this case and is assumed as needed.
|*)
Print freefor.
Print freefort.

(*|
Finally, we define three notions of complexity for qsp formulas
(Definition 3.3).
|*)
Print modaldepth. (* maximum number of nested diamonds *)
Print quantifierdepth. (* maximum number of nested quantifiers *)
Print depth. (* maximum number of nested symbols *)

(*|
QRC1
====
|*)

From QRC1 Require Import QRC1.

(*| The definition of QRC1 proof (Definition 3.1). |*)
Print QRC1Proof.
Locate "|- A ~> B".

(*| Lemma 3.2. |*)
Check AllC.
Check All_sub.
Check Diam_All.
Check alphaconversion.
Check TermIr.
Check Const_AllIr.

(*| Lemma 3.4. |*)
Check QRC1Proof_modaldepth.
Check Diam_irreflexive.

(*|
Kripke Semantics
================
|*)

From QRC1 Require Import KripkeSemantics.

(*| We only implement finite frames and models. |*)

(*|
Not-necessarily adequate frames and models (Definition 4.1).

We stipulate that the worlds of a frame are a finite set of some choiceType
named WType, and that the domain elements are a finite set of some choiceType
named MType. This restricts the implemented definition of frame to finite
frames, which is weaker than the definition presented in [1], but sufficient for
the completeness proof.

We use the notions of rawFrame and rawModel to refer to possibly non adequate
frames and models.

Unlike in [1], we define eta functions for every possible pair of worlds, which
avoids unnecessary dependent types at the level of rawFrames.
|*)
Print rawFrame.
Print rawModel.

(*|
We can now define adequate frames and models (Definition 4.2).

Note that since we require eta functions for every pair of worlds, we require an
additional property in order for a frame to be adequate, namely that all eta
functions between a world and itself be the identity.
|*)
Print frame.
  Print adequateF.
    Check transitiveFP.
    Check transetaFP.
    Check idetaFP.
Print model.
  Print adequateM.
    Check concordantMP.

(*|
An assignment is a function from variables to domain elements, parametrized on a
world. We also define the notion of preassignment, which is simply a function
from variables to some type T, as some definitions and results do not depend on
a world. These can be reused in contexts where a set of worlds has not been
defined yet (as happens during the Kripke completeness proof).
|*)
Print assignment.
Print preassignment.

(*| Assignments have a canonical extension to terms. |*)
Print assignment_of_term.
Locate "g `+".

(*|
A w-assignment can be lifted to a u-assignment by composing with eta u.

Again, we do not require that w R u as in [1], avoiding unwieldy dependent types
at this stage. In practice, we will only rely on this notation when w R u does
hold (see the definition of sat below).
|*)
Locate "g ` u".

(*|
Two preassignments are X-alternative (or just x-alternative when X = {x}) when
they agree on every variable except possibly those in X.
|*)
Print Xaltern.
Print xaltern.

(*| We now define the notion of satisfaction (Definition 4.3). |*)
Print sat.

(*|
Kripke soundness
----------------
|*)

From QRC1 Require Import KripkeSoundness.

(*| The soundness theorem (Theorem 4.4). |*)
Check soundness.
Print Assumptions soundness.

(*|
The soundness proof is reasonably straightforward and as such is left out of
[2]. However, it requires about 750 lines of extra definitions, lemmas, and
proofs. We summarize those in what follows, following the soundness proof
detailed in Section 4 of [1] `arXiv:2003.13651 [math.LO]
<https://arxiv.org/pdf/2003.13651.pdf>`_.
|*)

(*| Remark 4.5 |*)
Check sat_Xalternfv.

(*| Lemma 4.6 |*)
Check substitution_formula.


(*| Remark 4.7 |*)
Set Printing Implicit Defensive.
Check sat_noconstants.
Unset Printing Implicit Defensive.

(*|
We now work towards frames restricted to some world r and all its successors. We
follow a slightly different strategy than the one presented in [1], and as such
the intermediate lemmas don't exactly match. The idea is to replace the
interpretation of some constant c with some r-domain element d at the world r
and by eta u d at every successor of r. Since this breaks the concordance of the
model, we first drop every other world, obtaining a model that satisfies exactly
the same formulas at r. Here the main difference is that we do not immediately
drop the extra worlds; we keep them around and change the adequateness condition
instead, so that it checks only r and its successors. We then proceed with this
not entirely adequate model and only after the replacement is done do we drop
the extra worlds and show that the resulting model is fully adequate. This
strategy proved much easier to implement than dropping the extra worlds first.
|*)

(*| The reflexive closure of R. |*)
Print Rs.

(*|
A different notion of adequacy, which only checks a root world and its
successors.
|*)
Print adequateFr.
  Check transitiveFrP.
  Check transetaFrP.
  Check idetaFrP.
Print adequateMr.
  Check concordantMrP.

(*|
An adequate frame is also adequate in the adequateFr sense, and the same for
models.
|*)
Check frame_adequateFr.
Check model_adequateMr.

(*|
Given some interpretation I, replace_I is a new interpretation that behaves as I
except it interprets a given constant c as a given r-domain element d instead.
This new interpretation will not lead to an adequate model unless r is the root
(the concordance condition may fail).
|*)
Print replace_I.
Print replace_rawModel.

Set Printing Implicit Defensive.

(*|
We use the following notation to refer to raw models where the interpretation of
c has been replaced by d on world r and its successors.
|*)
Locate "M `[ r , c <- d ]".

(*|
A model replacing some constant interpretation with an element of the domain of
r is adequate if ignoring every world that is not either r or its successor.
|*)
Check replace_adequateMr.

(*|
Changing the interpretation of a constant that does not appear in A is
irrelevant.
|*)
Check sat_replace_noconstants.

(*|
Checking whether A is satisfied at world u is independent of which root is
picked, as long as u is either the root or a successor of the root.
|*)
Check sat_replace_root.
(*|
In particular, we can pick the world where we want to check A as the root.
|*)
Check sat_replace_root_self.

(*|
A is satisfied at w in a model M iff A[x <- Const c] is satisfied at w in M with
the interpretation of c replaced by the interpretation of x This is not quite
Lemma 4.12 yet, because the new model is not adequate.
|*)
Check sat_replace.

(*| We now start working toward actually dropping the useless worlds. |*)

(*| A restricted set of worlds. |*)
Print restrict_world.

(*| We can now define restricted raw frames and raw models (Definition 4.8). |*)
Print restrict_rawFrame.
  Print restrict_R.
  Print restrict_domain.
  Print restrict_eta.
Print restrict_rawModel.
  Print restrict_I.
  Print restrict_J.

(*|
Restricted raw frames and models are adequate in the normal sense (Remark 4.9).
|*)
Check restrict_frame_adequate.
Check restrict_model_adequate.

(*| And thus we can define restricted frames and models. |*)
Print restrict_frame.
Print restrict_model.

(*| Remark 4.10 |*)
Check sat_restrict.

(*|
Restricting M`[r, c <- d] to r and its successors gives us an adequate model.
|*)
Check restrict_replace_adequate.

(*| Definition 4.11 |*)
Print restrict_replace.

(*|
We can finally prove the key lemma for the soundness of ConstE (Lemma 4.12).
|*)
Check sat_restrict_replace.

Unset Printing Implicit Defensive.

(*|
Kripke completeness
-------------------
|*)

From QRC1 Require Import Closure Pairs KripkeCompleteness.

(*| A pair is just a pair of finite sets of formulas. |*)
Print pair.
  Print formulas.

(*| The notion of Phi-MCW is defined here as wfpair. |*)
Print wfpair.
  Print closedfs.
  Print consistent.
  Check maximalP.
  Check witnessedP.

(*|
The closed closure of a formula is defined by induction on the depth of the
formula (Definition 5.1). The closure of a set of formulas is the union of the
closures of the individual formulas.
|*)
Check closure_eq.
Print closurefs.

(*|
The number of different constants in a formula and the maximum of this value for
a set of formulas (Definition 5.2).
|*)
Print constantcount.
Print constantcountfs.

(*| Remark 5.3 |*)
Check constantcount_sub.
Check constantcount_closure.
Check constantcount_closurefs.

(*|
From now on we can no longer continue without classical logic. Thus we depend on
the excluded middle axiom.
|*)
Check Classical_Prop.classic.

(*| The Lindenbaum Lemma (Lemma 5.4) |*)
Check lindenbaum.

(*|
We define RR as the target relation between worlds of the canonical model
(Definition 5.5).
|*)
Check RRP.

(*|
RR is transitive and irreflexive (when restricted to consistent pairs) (Lemma
5.6).
|*)
Check RR_trans.
Check RR_irr.

(*| The pair existence lemma (Lemma 5.7) |*)
Check pair_existence.

(*| Canonical model (Definition 5.8) |*)
(*|
The canonical model depends on two formulas, PP and NN. Later we will assume
that ~ (\|- PP ~> NN), but this is not required for the definition of the
canonical model itself. It further depends on the set of canonical worlds, which
is left as a parameter at this stage, since we can't constructively derive it.
|*)
Print crawFrame.
  Print cR.
  Print cdomain.
Print crawModel.
  Print cdomain_of_Const.
  Print cJ.
Check crawModel_adequate.
Print cmodel.


(*| Assignify (Definition 5.9) |*)
Print assignify.
Print assignify_exs.

(*| Truth lemma (Lemma 5.10) |*)
Check truth.

(*| The completeness theorem (Theorem 5.11) |*)
Check completeness.
Print Assumptions completeness.

(*|
Decidability
------------
|*)

From QRC1 Require Import Decidability.

(*|
We prove decidability of QRC1 classically via Post's Theorem. To this end, we
first show that both \|- A ~> B and ~ (\|- A ~> B) are enumerable. We use an
implementation of Post's Theorem and enumerability provided by the Coq Library
of Undecidability Proofs (CLUP). More precisely, we use a weaker form of Post's
Theorem that assumes the axiom of exluded middle instead of only assuming
Markov's Principle. This distinction is irrelevant in our case because we
already assume the axiom of excluded middle for other results required by the
decidability proof.
|*)

(*| Enumerability (CLUP) |*)
Print enumerable.rec_enum_t.

(*| (Weak) Post's Theorem (CLUP) |*)
Check enumerable.bi_rec_enum_t_dec.

(*| \|- A ~> B is enumerable |*)
Check QRC1Proof_enum.
  Print QRC1Proof_pair.

(*| ~ (\|- A ~> B) is enumerable |*)
Check nQRC1Proof_enum.

(*| QRC1 is decidable |*)
Check QRC1Proof_dec.
Print Assumptions QRC1Proof_dec.

(*|
In order to show enumerability of \|- A ~> B, we define the concepts of
n-bounded term, n-bounded formula, and n-bounded proof.
|*)

(*|
An n-bounded term is a term together with a proof that its size is at most n.
Note that the size of a constant is 0 and the size of a variable is the value of
that variable.
|*)
Print bterm.
  Print tsize.
  Print termfv.

(*| The set of n-bounded terms is clearly finite. |*)
Check [finType of bterm _ _].

(*|
An n-bounded formula is a raw n-bounded formula together with a proof that its
depth at most n. A raw n-bounded formula is defined like a regular formula,
except all its terms are n-bounded, including the quantified-over variables.
|*)
Print bformula.
  Print rawBformula.

(*|
Like with terms, the set of n-bounded formulas is finite. However, this proof is
slightly more envolved. We enumerate all the raw n-bounded formulas with depth
at most n and then prove that this is exactly the set of n-bounded formulas.
|*)
Print bformulas_bdepth.
Check [finType of bformula _ _].

(*|
An n-bounded proof is defined similarly to a QRC1-proof, except every term and
formula not present in the conclusion of a rule is taken to be n-bounded and the
overall depth of the proof is at most n. Furthermore, there is a new rule to the
effect that every n-bounded proof is also an n.+1-bounded proof.
|*)
Print bQRC1Proof.
Locate "|- n ` A ~> B".

(*|
The term introduction rule bTermI is a special case: even though A, B, x and t
all appear in the conclusion of the rule, they do so in the form A`[Var x <- t]
and B`[Var x <- t], which is hard to identify in general. Thus we require that
all four objects be n-bounded in this case.
|*)
Check bTermI.

(*|
We now define a boolean predicate over a number n and formulas A and B testing
whether there is an n-bounded proof of A ~> B and prove that this is the case if
and only if \|-n` A ~> B. This implies the decidability of \|-n` A ~> B.
|*)
Print bQRC1Proof_bool.
Check bQRC1ProofP.

(*|
We can finally show that \|- A ~> B if and only if there exists some bound n
such that \|-n` A ~> B, which implies the enumerability of \|- A ~> B.
|*)
Check QRC1Proof_bQRC1Proof.

(*|
In order to show the enumerability of ~ (\|- A ~> B), we make use of the
soundness and completeness results for QRC1: ~ (\|- A ~> B) if and only if there
is a world in the canonical model where A is satisfied and B is not. Thus, we
just need to enumerate the worlds of the canonical model for A and B and check
whether A and B are satisfied there.
|*)

(*|
Given a number n and formulas A and B, we can check whether n is the code of a
world of the canonical model satisfying A and not satisfying B. More precisely,
recall that the set of worlds of the canonical model is not explicitly defined,
only non-constructively proved to exist. Thus, we actually check whether n is
the code of a set of worlds for the canonical model together with a choice of
specific world w where A is satisfied and B isn't. We also need to make use of a
boolean satisfaction relation instead of our usual Prop one.
|*)
Print enumerate_cmodels.
  Print satb.
  Check satP.
