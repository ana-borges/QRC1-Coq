From mathcomp Require Import all_ssreflect finmap.

From QRC1 Require Import Language.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Reserved Notation "g `+" (at level 1, format "g `+").
Reserved Notation "g ` u" (at level 1, format "g ` u").

Section KripkeSemantics.

Open Scope qsp_scope.
Open Scope fset.

Variable sig : signature.
Notation term := (term sig).
Notation formula := (formula sig).
Notation ConstName := (ConstName sig).
Notation PredName := (PredName sig).
Notation Var := (Var sig).

Variables WType MType : choiceType.

(* Note: we only implement finite frames *)
Record rawFrame := RawFrame {
  world :> {fset WType};
  R : world -> world -> bool;
  domain : world -> {fset MType};
  eta : forall (w u : world), domain w -> domain u;
  (* at the level of rawFrames we require an eta function for every pair w, u *)
}.

Record rawModel := RawModel {
  rawFrame_of_rawModel :> rawFrame;
  I : forall w : world rawFrame_of_rawModel, ConstName -> (domain w);
  J : forall w : world rawFrame_of_rawModel, forall (P : PredName),
        {fset (arity P).-tuple (domain w)}
}.

Definition transitiveF (F : rawFrame) : bool :=
  [forall w : F, forall u : F, forall v : F,
    (R w u) ==> (R u v) ==> (R w v)
  ].

Lemma transitiveFP (F : rawFrame) :
  reflect (transitive (@R F)) (transitiveF F).
Proof.
  by apply: (iffP 'forall_'forall_'forall_implyP)=>H????; apply/implyP; apply:H.
Qed.

Definition transetaF (F : rawFrame) : bool :=
  [forall w : F, forall u : F, forall v : F, forall d : domain w,
      (R w u) ==> (R u v) ==>
    (eta v d == eta v (eta u d))
  ].

Lemma transetaFP (F : rawFrame) :
  reflect
    (forall (w u v : F) (d : domain w),
        R w u -> R u v ->
      eta v d = eta v (eta u d)
    )
    (transetaF F).
Proof.
  apply: (iffP 'forall_'forall_'forall_'forall_implyP) => /= H ? ? ? ? ?.
    by move=> Ruv; apply/eqP; move: Ruv; apply/implyP; apply: H.
  by apply/implyP => ?; apply/eqP; apply: H.
Qed.

Definition idetaF (F : rawFrame) : bool :=
  [forall w : F, forall d : domain w, eta w d == d].

Lemma idetaFP (F : rawFrame) :
  reflect
    (forall (w : F) (d : domain w), eta w d = d)
    (idetaF F).
Proof. by apply: (iffP 'forall_'forall_eqP). Qed.

Definition concordantM (M : rawModel) : bool :=
  [forall w : M, forall u : M, (R w u) ==>
     [forall c : ConstName, val (I u c) == val (eta u (I w c))]
  ].

Lemma concordantMP (M : rawModel) :
  reflect
    (forall (w u : M), R w u ->
        forall c : ConstName, val (I u c) = val (eta u (I w c))
    )
    (concordantM M).
Proof.
  by apply: (iffP 'forall_'forall_implyP)=> H ? ? ?; apply/'forall_eqP; apply:H.
Qed.

Definition adequateF (F : rawFrame) : bool :=
  [&& transitiveF F, transetaF F & idetaF F].

Lemma adequateFP (F : rawFrame) :
  reflect [/\ transitiveF F, transetaF F & idetaF F] (adequateF F).
Proof. by apply: (iffP and3P). Qed.

Record frame := Frame {
  rawFrame_of_frame :> rawFrame;
  _ : adequateF rawFrame_of_frame
}.
Canonical frame_subType := [subType for rawFrame_of_frame].

Lemma frame_adequate (F : frame) : adequateF F.
Proof. by case: F. Qed.

Definition adequateM (M : rawModel) : bool :=
  (adequateF M) && (concordantM M).

Lemma adequateMP (M : rawModel) :
  reflect (adequateF M /\ concordantM M) (adequateM M).
Proof. by apply: (iffP andP). Qed.

Record model := Model {
  rawModel_of_model :> rawModel;
  _ : adequateM rawModel_of_model
}.
Canonical model_subType := [subType for rawModel_of_model].

Lemma model_adequate (M : model) : adequateM M.
Proof. by case: M. Qed.

Lemma model_concordant (M : model) : concordantM M.
Proof. by move/adequateMP: (model_adequate M) => []. Qed.

Lemma frame_of_model_subproof (M : model) : adequateF M.
Proof.
  case: M => F adeqF; rewrite /rawModel_of_model.
  by move /adequateMP : adeqF => [].
Qed.
Hint Resolve frame_of_model_subproof : core.

Coercion frame_of_model (M : model) := @Frame M (frame_of_model_subproof M).
(* We define frame_of_model as canonical because the square below commutes *)
(*
   model --> rawModel
    |         |
    |    o    |
    v         v
   frame --> rawFrame
*)
Canonical frame_of_model.

Section Preassignment.

Variable T : Type.

Definition preassignment : Type := VarName -> T.

Definition Xaltern (g h : preassignment) (X : {fset VarName}) : Prop :=
  forall x : VarName, x \notin X -> g x = h x.

Definition xaltern (g h : preassignment) (x : VarName) : Prop :=
  Xaltern g h [fset x].

Lemma XalternC (g h : preassignment) (X : {fset VarName}) :
  Xaltern g h X <-> Xaltern h g X.
Proof.
  move: g h.
  suff : forall g h, Xaltern g h X -> Xaltern h g X.
    by move=> Himpl; split; apply: Himpl.
  by move=> g h; rewrite /Xaltern => alterngh x xnotinX; rewrite alterngh.
Qed.

Lemma XalternU (g h : preassignment) (X Y : {fset VarName}) :
  Xaltern g h X -> Xaltern g h (X `|` Y).
Proof.
  move=> Xalterngh x.
  by rewrite in_fsetE negb_or => /andP [xnotinX _]; apply: Xalterngh.
Qed.

Lemma XalternW (g h : preassignment) (X Y : {fset VarName}) :
    X `<=` Y ->
  Xaltern g h X -> Xaltern g h Y.
Proof. by move=> /fsetUidPr <- Xalterngh; apply: XalternU. Qed.

Lemma Xaltern_trans (g1 g2 g3 : preassignment) (X : {fset VarName}) :
    Xaltern g1 g2 X ->
    Xaltern g2 g3 X ->
  Xaltern g1 g3 X.
Proof.
  move=> Xalterng1g2 Xalterng2g3 x xnotinX.
  by rewrite Xalterng1g2 // Xalterng2g3.
Qed.

Definition Xeq (g h : preassignment) (X : {fset VarName}) : Prop :=
  forall x : VarName, x \in X -> g x = h x.

Lemma XeqC (g h : preassignment) (X : {fset VarName}) :
  Xeq g h X <-> Xeq h g X.
Proof.
  move: g h.
  suff : forall g h, Xeq g h X -> Xeq h g X.
    by move=> Himpl; split; apply: Himpl.
  by move=> g h; rewrite /Xeq => eqgh x xinX; rewrite eqgh.
Qed.

Lemma Xeq_trans (g1 g2 g3 : preassignment) (X : {fset VarName}) :
    Xeq g1 g2 X ->
    Xeq g2 g3 X ->
  Xeq g1 g3 X.
Proof.
  move=> Xeqg1g2 Xeqg2g3 x xinX.
  by rewrite Xeqg1g2 // Xeqg2g3.
Qed.

Lemma XeqW (g h : preassignment) (X Y : {fset VarName}) :
    X `<=` Y ->
  Xeq g h Y -> Xeq g h X.
Proof.
  move=> /fsetIidPl <- Yeqgh x xinX; rewrite Yeqgh //.
  by move: xinX; rewrite in_fsetE => /andP [].
Qed.

Definition preassignment_sub (g : preassignment) (x : VarName) (t : T)
    : preassignment :=
  fun y => if y == x then t else g y.
Notation "g `[ x <~ t ]" := (preassignment_sub g x t)
  (at level 8, format "g `[ x  <~  t ]").

Lemma preassignment_sub_xaltern (g : preassignment) (x : VarName) (t : T) :
  xaltern g g`[x <~ t] x.
Proof. by rewrite /preassignment_sub => y /[!inE] /negPf ->. Qed.

Lemma preassignment_sub_eq1 (g h : preassignment) (x : VarName) :
    xaltern g h x ->
  g`[x <~ h x] =1 h.
Proof.
  rewrite /preassignment_sub => xalterngh y.
  case: eqP => [->//|neqyx].
  apply: xalterngh.
  by rewrite !inE; apply/eqP.
Qed.

End Preassignment.

Notation "g `[ x <~ t ]" := (preassignment_sub g x t)
  (at level 8, format "g `[ x  <~  t ]").

Definition assignment (F : rawFrame) (w : F) : Type := preassignment (domain w).

Definition assignment_of_term (M : rawModel) (w : M) (g : assignment w)
    : term -> domain w := fun t =>
  match t with
  | Language.Var x => g x
  | Const c => I w c
  end.
Notation "g `+" := (assignment_of_term g).

Notation "g ` u" := (eta u \o g).

Lemma Xaltern_eta (F : rawFrame) (w u : F) (g h : assignment w)
    (X : {fset VarName}) :
  Xaltern g h X -> Xaltern g`u h`u X.
Proof.
  move=> alterngh x xnotinX.
  by apply/val_eqP => /=; apply/val_eqP; rewrite alterngh.
Qed.

Lemma Xeq_eta (F : rawFrame) (w u : F) (g h : assignment w)
    (X : {fset VarName}) :
  Xeq g h X -> Xeq g`u h`u X.
Proof.
  move=> Xeqgh x xinX.
  by apply /val_eqP => /=; apply /val_eqP; rewrite Xeqgh.
Qed.

Fixpoint sat (M : rawModel) (w : M) (g : assignment w) (A : formula) : Prop :=
  match A with
  | T => True
  | Pred P ts => [tuple of map g`+ ts] \in J w P
  | B /\ C => sat g B /\ sat g C
  | <> B => exists2 u : M, R w u & sat g`u B
  | All x B => forall h : assignment w, xaltern g h x -> sat h B
  end.

Lemma sat_Xeqfv (M : rawModel) (w : M) (g h : assignment w) (X : {fset VarName})
    (A : formula) :
    Xeq g h (fv A) ->
  sat g A <-> sat h A.
Proof.
  move: M w g h.
  suff: forall g h, Xeq g h (fv A) -> sat g A -> sat h A.
    move=> Himpl alterngh; split.
      by apply: Himpl.
    by apply: Himpl => //; rewrite XeqC.
  move=> M; elim: A => //.
  - move=> P ts w g h /= Peqgh.
    suff -> : [tuple of map g`+ ts] = [tuple of map h`+ ts] by [].
    apply: eq_from_tnth => i; rewrite 2!tnth_map.
    case eqtnth : (tnth ts i) => [x /= | //].
    rewrite Peqgh //; apply /bigfcupP => /=.
    exists (Var x) => //=.
      by rewrite -eqtnth mem_tnth.
    by rewrite fset11.
  - move=> A IHA B IHB w g h /= ABeqgh [satgA satgB]; split.
      apply: (IHA _  g) => //.
      by apply: (XeqW _ ABeqgh); apply: fsubsetUl.
    apply: (IHB _  g) => //.
    by apply: (XeqW _ ABeqgh); apply: fsubsetUr.
  - move=> A IHA w g h /= Aeqgh [u Rwu satgA].
    exists u => //.
    apply: (IHA _ g`u) => //.
    by apply: Xeq_eta.
  - move=> y A IHA w g h yAeqgh /= satgyA f yalternhf.
    pose f' := fun z : VarName => if z == y then f y else g z.
    have yalterngf' : xaltern g f' y.
      by move=> z; rewrite /f' in_fsetE; case: eqP.
    have yAeqhf : Xeq h f (fv (All y A)).
      move=> x xinfvyA; rewrite yalternhf //.
      by move: xinfvyA; rewrite /= !in_fsetE => /andP [].
    move: (satgyA _ yalterngf'); apply: IHA => x xinA.
    rewrite /f'; case: eqP => [-> // | /eqP neqxy].
    have xinyA : x \in fv (All y A).
      by rewrite /= inE xinA in_fsetE neqxy.
    by rewrite -yAeqhf // yAeqgh.
Qed.

Lemma sat_Xalternfv (M : rawModel) (w : M) (g h : assignment w)
    (X : {fset VarName}) (A : formula) :
    Xaltern g h X ->
    fdisjoint X (fv A) ->
  sat g A <-> sat h A.
Proof.
  move=> Xalterngh /fdisjointP_sym disjXA; apply: (sat_Xeqfv X).
  move=> x xinA; rewrite Xalterngh //.
  by apply: disjXA.
Qed.

Lemma sat_eq1 (M : rawModel) (w : M) (g h : assignment w) (A : formula) :
  g =1 h -> sat g A <-> sat h A.
Proof.
  move=> eqgh.
  apply: (sat_Xalternfv (X := fset0)) => //.
  exact: fdisjoint0X.
Qed.

Fixpoint satb (M : rawModel) (w : M) (g : assignment w) (A : formula)
    : bool :=
  match A with
  | T => true
  | Pred P ts => [tuple of map g`+ ts] \in J w P
  | B /\ C => satb g B && satb g C
  | <> B => [exists u : M, R w u && satb g`u B]
  | All x B => [forall d : domain w, satb g`[x <~ d] B]
  end.

Lemma satP (M : rawModel) (w : M) (g : assignment w) (A : formula) :
  reflect (sat g A) (satb g A).
Proof.
  elim: A w g => /=.
  - by move=> w g; apply: (iffP idP).
  - by move=> P ts w g; apply: (iffP idP).
  - by move=> A IHA B IHB w g; apply: (iffP andP) => -[/IHA + /IHB].
  - move=> A IHA w g.
    apply: (iffP 'exists_andP) => /=.
      by move=> [u [Rwu /IHA satuA]]; exists u.
    by move=> [u Rwu /IHA satuA]; exists u.
  - move=> x A IHA w g.
    apply: (iffP 'forall_idP) => /=.
      move=> Hdom h xalterngh.
      move: (Hdom (h x)) => /IHA /(sat_eq1 (h := h)).
      by apply; apply: preassignment_sub_eq1.
    move=> Hassg d.
    by apply/IHA; apply: Hassg; apply: preassignment_sub_xaltern.
Qed.

Lemma substitution_formula (M : model) (w : M) (g g' : assignment w)
    (x : VarName) (t : term) (A : formula) :
    xaltern g g' x ->
    g' x = g`+ t ->
    freefor A x t ->
  sat g' A <-> sat g A`[Var x <- t].
Proof.
  have [| xnotinA] := boolP (x \in fv A); last first.
    move=> xalterngg' _ _.
    rewrite sub_notfree // (sat_Xalternfv xalterngg') //.
    by apply /fdisjointP => /= y; rewrite in_fsetE => /eqP ->.
  move: w g g'; elim: A => //.
  - move=> P ts w g g' /= xinP xalterngg' eqg'xgt _.
    set mapg := [tuple of map g`+ _].
    suff -> : [tuple of map g'`+ ts] = mapg by [].
    apply: eq_from_tnth => i; rewrite 3!tnth_map.
    case: eqP => [-> // |].
    case: (tnth ts i) => //= y neqyx.
    rewrite xalterngg' // in_fsetE.
    by apply /eqP => eqyx; move: neqyx; rewrite eqyx.
  - move=> A IHA B IHB w g g' /=.
    rewrite in_fsetE => /orP [xinA | xinB] xalterngg' eqg'xgt.
      move=> /andP [freeforA freeforB].
      rewrite (IHA _ g) //.
      have [xinB | xnotinB] := boolP (x \in fv B).
        by rewrite (IHB _ g).
      rewrite (sub_notfree _ xnotinB).
      rewrite (@sat_Xalternfv _ _ _ _ _ B xalterngg') //.
      by apply /fdisjointP => /= y; rewrite in_fsetE => /eqP ->.
    move=> /andP [freeforA freeforB].
    rewrite (IHB _ g) //.
    have [xinA | xnotinA] := boolP (x \in fv A).
      by rewrite (IHA _ g).
    rewrite (sub_notfree _ xnotinA).
    rewrite (@sat_Xalternfv _ _ _ _ _ A xalterngg') //.
    by apply /fdisjointP => /= y; rewrite in_fsetE => /eqP ->.
  - move=> A IHA w g g' /= xinA xalterngg' eqg'xgt freeforAxt.
    have xalterngRg'R : forall u, xaltern g`u g'`u x.
      by move=> u; apply: Xaltern_eta.
    have eqg'RxgRt : forall u, R w u -> g'`u x = (g`u)`+ t.
      move=> u Rwu; apply /val_eqP => /=; rewrite eqg'xgt.
      case: {+}t => //= c.
      move: (model_adequate M) => /adequateMP [_ /concordantMP].
      by move=> /(_ _) /(_ _) /(_ Rwu) ->.
    split; move=> [u Rwu satuA]; exists u => //.
      by rewrite -(IHA _ _ g'`u) // eqg'RxgRt.
    by rewrite (IHA _ g`u) // eqg'RxgRt.
  - move=> y A IHA w g g' xinyA xalterngg' eqg'xgt freeforyAxt.
    have neqyx : y == x = false.
      by apply /eqP => eqxy; move: xinyA => /=; rewrite eqxy 2!inE eqxx.
    have neqVaryx : Var y == Var x = false.
      by apply/eqP => -[eqyx]; move: neqyx; rewrite eqyx eqxx.
    have xinA : x \in fv A.
      by move: xinyA => /=; rewrite !in_fsetE => /andP [].
    have freeforAxt : freefor A x t.
      by move: freeforyAxt => /= /orP [| /andP [] //]; rewrite neqyx.
    split.
      move=> satg'yA /=; rewrite neqVaryx => /= h yalterngh.
      pose h' := fun z : VarName => if z == x then h`+ t else h z.
      have xalternhh' : xaltern h h' x.
        by move=> z; rewrite in_fsetE /h' => /negPf ->.
      rewrite -(IHA _ _ h') //; last by rewrite /h' eqxx.
      apply: satg'yA.
      have xyalternhh' : Xaltern h h' [fset x; y].
        by apply: (XalternW _ xalternhh'); rewrite fsub1set 2!inE eqxx.
      have xyalterngh : Xaltern g h [fset x; y].
        by apply: (XalternW _ yalterngh); rewrite fsub1set 3!inE eqxx orbT.
      have xyalterng'g : Xaltern g' g [fset x; y].
        rewrite XalternC.
        by apply: (XalternW _ xalterngg'); rewrite fsub1set 2!inE eqxx.
      have xyalterng'h' : Xaltern g' h' [fset x; y].
        by apply: (Xaltern_trans xyalterng'g); apply: (Xaltern_trans xyalterngh).
      move=> z; rewrite in_fsetE => /negPf neqzy.
      case: (@eqP _ z x) => [-> | /eqP /negPf neqzx]; last first.
        by rewrite xyalterng'h' // !in_fsetE neqzy neqzx.
      rewrite /h' eqxx eqg'xgt.
      have : t != Var y.
        apply /eqP => eqty; move: freeforyAxt.
        by rewrite /= neqyx freeforAxt xinA eqty in_fsetE eqxx.
      case: {+}t => //= z' neqz'y.
      rewrite yalterngh // in_fsetE.
      by apply /eqP => eqz'y; move: neqz'y; rewrite eqz'y eqxx.
    rewrite /= neqVaryx => /= satgyAt h' yalterng'h'.
    pose h := fun z : VarName => if z == x then g x else h' z.
    have xalternhh' : xaltern h h' x.
      by move=> z; rewrite in_fsetE /h => /negPf ->.
    have yalterngh : xaltern g h y.
      have xyalternh'h : Xaltern h' h [fset x; y].
        rewrite XalternC.
        by apply: (XalternW _ xalternhh'); rewrite fsub1set 2!inE eqxx.
      have xyalterngg' : Xaltern g g' [fset x; y].
        by apply: (XalternW _ xalterngg'); rewrite fsub1set 2!inE eqxx.
      have xyalterng'h' : Xaltern g' h' [fset x; y].
        apply: (XalternW _ yalterng'h').
        by rewrite fsub1set 3!inE eqxx orbT.
      have xyalterngh : Xaltern g h [fset x; y].
        apply: (Xaltern_trans xyalterngg').
        by apply: (Xaltern_trans xyalterng'h').
      move=> z; rewrite in_fsetE => /negPf neqzy.
      case: (@eqP _ z x) => [-> | /eqP /negPf neqzx]; first by rewrite /h eqxx.
      by rewrite xyalterngh // !in_fsetE neqzy neqzx.
    rewrite (IHA _ h) //; first by apply: satgyAt.
    rewrite -yalterng'h'; last by rewrite in_fsetE eq_sym neqyx.
    rewrite eqg'xgt.
    have : t <> Var y.
      move=> eqty; move: freeforyAxt.
      by rewrite /= freeforAxt xinA eqty neqyx in_fsetE eqxx.
    case: {+}t => //= z neqzy.
    rewrite yalterngh // in_fsetE.
    by apply /eqP => eqzy; apply: neqzy; rewrite eqzy.
Qed.

Lemma sat_noconstants (F : rawFrame) Ic Ic' J :
  let rM := @RawModel F Ic J in
  let rM' := @RawModel F Ic' J in
  forall (adeqM : adequateM rM) (adeqM' : adequateM rM') (w : F)
      (g : assignment w) (A : formula),
    (forall c, c \in constants A -> Ic w c = Ic' w c) ->
    sat (M := Model adeqM) g A <-> sat (M := Model adeqM') g A.
Proof.
  move=> rM rM' adeqM adeqM' w g A; move: w g; elim: A => //=.
  - move=> P ts w g constantfree.
    set tup := [tuple of _].
    set tup' := [tuple of _].
    suff -> : tup = tup' by [].
    apply: eq_from_tnth => i; rewrite 2!tnth_map.
    case eqtnth : (tnth ts i) => [// | c /=].
    apply: constantfree; apply /bigfcupP => /=; exists (Const c).
      by rewrite -eqtnth mem_tnth.
    by rewrite /= in_fsetE eqxx.
  - move=> A IHA B IHB w g constantfreeAB.
    rewrite IHA ?IHB //.
      move=> c cinB; apply: constantfreeAB.
      by rewrite in_fsetU cinB orbT.
    move=> c cinA; apply: constantfreeAB.
    by rewrite in_fsetU cinA.
  - move=> A IHA w g constantfree.
    split.
      move=> [u Rwu satgA]; exists u => //.
      rewrite -IHA // => c cinA.
      have := adeqM => /adequateMP [_ /concordantMP /(_ _) /(_ _) /(_ Rwu)].
      move=> /(_ c) /= eqIwcIuc; apply /val_eqP => /=; rewrite eqIwcIuc.
      have := adeqM' => /adequateMP [_ /concordantMP /(_ _) /(_ _) /(_ Rwu)].
      move=> /(_ c) /= eqI'wcI'uc; rewrite eqI'wcI'uc.
      by rewrite constantfree.
    move=> [u Rwu satgA]; exists u => //.
    rewrite IHA // => c cinA.
    have := adeqM => /adequateMP [_ /concordantMP /(_ _) /(_ _) /(_ Rwu)].
    move=> /(_ c) /= eqIwcIuc; apply /val_eqP => /=; rewrite eqIwcIuc.
    have := adeqM' => /adequateMP [_ /concordantMP /(_ _) /(_ _) /(_ Rwu)].
    move=> /(_ c) /= eqI'wcI'uc; rewrite eqI'wcI'uc.
    by rewrite constantfree.
  - move=> x A IHA w g constantfree; split.
      by move=> satgxA h xaltengh; rewrite -IHA //; apply: satgxA.
    by move=> satgxA h xaltengh; rewrite IHA //; apply: satgxA.
Qed.

End KripkeSemantics.

Notation "g `+" := (assignment_of_term g).
Notation "g ` u" := (eta u \o g).

Section ModelUnlift.

Variable sig : signature.
Variable n : nat.
Variables (WType MType : choiceType).

Notation sign := (extend sig n).
Notation rawModeln := (rawModel sign WType MType).
Notation rawModel := (rawModel sig WType MType).
Notation modeln := (model sign WType MType).
Notation model := (model sig WType MType).

Definition rawModel_unlift (M : rawModeln) : rawModel :=
  {|
    rawFrame_of_rawModel := M;
    I := fun w (c : ConstName sig) => I w (inl c);
    J := J (r := M)
  |}.

Lemma model_unlift_subproof (M : modeln) :
  adequateM (rawModel_unlift M).
Proof.
  apply/adequateMP; split.
    by apply: frame_adequate.
  move/concordantMP: (model_concordant M) => concM.
  apply/concordantMP => w u Rwu c.
  by rewrite /I /=; apply: concM.
Qed.

Definition model_unlift (M : modeln) : model :=
  Model (model_unlift_subproof M).

Open Scope qsp_scope.

Lemma sat_unlift (M : rawModeln) (w : M) (g : assignment w) (A : formula sig) :
  sat (M := rawModel_unlift M) g A <-> sat g A!!n.
Proof.
  elim: A w g => //=.
  - move=> P ts w g.
    set gts := [tuple of _]; set gtsl := [tuple of _].
    suff -> : gts = gtsl by [].
    apply: eq_from_tnth => i; rewrite !tnth_map.
    by case: (tnth ts i).
  - by move=> A IHA B IHB w g; rewrite IHA IHB.
  - move=> A IHA w g.
    by split=> -[u Rwu satA]; exists u => //; [rewrite -IHA | rewrite IHA].
  - move=> x A IHA w g.
    by split => H h xgh; [rewrite -IHA | rewrite IHA]; apply: H.
Qed.

End ModelUnlift.
