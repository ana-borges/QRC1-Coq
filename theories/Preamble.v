From mathcomp Require Import all_ssreflect finmap.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Section Finmap.

Open Scope fset.

Variables (T K V : choiceType) (f : K -> V).

Lemma fset_seq1 (A : T) :
  [fset A] = [:: A] :> seq T.
Proof.
  apply: perm_small_eq => //.
  apply: uniq_perm => //.
  by move=> B; rewrite 2!inE.
Qed.

Lemma imfsetU (K1 K2 : {fset K}) :
  [fset f k | k in K1 `|` K2] = [fset f k | k in K1] `|` [fset f k | k in K2].
Proof.
  apply/fsetP => v; apply/imfsetP/fsetUP => /=.
    move=> [k /fsetUP [kinK1 -> | kinK2 ->]].
      by left; apply/imfsetP; exists k.
    by right; apply/imfsetP; exists k.
  move=> [|] /imfsetP /= [k].
    by move=> kinK1 ->; exists k => //; rewrite inE kinK1.
  by move=> kinK2 ->; exists k => //; rewrite inE kinK2 orbT.
Qed.

Lemma imfset_bigfcup (F : T -> {fset K}) (ts : {fset T}) :
  [fset f k | k in \bigcup_(t <- ts) F t] =
    \bigcup_(t <- ts) [fset f k | k in F t].
Proof.
  apply/fsetP => v.
  apply/imfsetP/bigfcupP => /=.
    move=> [k /bigfcupP [t tintsT kinFt] ->].
    exists t => //.
    by apply/imfsetP => /=; exists k.
  move=> [t tintsT /imfsetP /= [k kinFt ->]].
  exists k => //=.
  by apply/bigfcupP; exists t.
Qed.

Lemma imfset0 : [fset f k | k in fset0] = fset0.
Proof.
  apply/fsetP => v; apply/imfsetP.
  by rewrite inE => /= -[k /[!inE]].
Qed.

Lemma imfset1 (k : K) : [fset f k' | k' in [fset k]] = [fset f k].
Proof.
  apply/fsetP => k'; apply/imfsetP/fset1P => /=.
    by move=> [k'' /fset1P ->].
  by move=> ->; exists k; rewrite ?in_fsetE.
Qed.

Lemma imfset2 (k1 k2 : K) :
  [fset f k' | k' in [fset k1; k2]] = [fset f k1; f k2].
Proof. by rewrite imfsetU 2!imfset1. Qed.

End Finmap.

Lemma existsb (T : finType) (P : {pred T}) (x : T) : P x -> [exists x, P x].
Proof. by move=> Px; apply/existsP; exists x. Qed.
Arguments existsb {T P}.

Section Misc.

Open Scope fset.

Lemma all_fsetU (T : choiceType) a (A B : {fset T}) :
  all a (A `|` B) = (all a A) && (all a B).
Proof.
  apply/allP/andP.
    move=> allAUB; split; apply/allP.
      by move=> x xinA; apply: allAUB; rewrite inE xinA.
    by move=> x xinB; apply: allAUB; rewrite inE xinB orbT.
  move=> [/allP allA /allP allB] x /fsetUP [xinA | xinB].
    by apply: allA.
  by apply: allB.
Qed.

Lemma imfset_f (T1 T2 : choiceType) (f : T1 -> T2) (s : {fset T1}) (x : T1):
    x \in s ->
  f x \in [fset f i | i in s].
Proof. by move=> xins; apply/imfsetP; exists x. Qed.

Variable (R : Type) (idx : R) (op : Monoid.com_law idx).
Variable (I J : choiceType).

Lemma big_fsetU (A B : {fset I}) (F : I -> R) :
    idempotent op ->
  \big[op/idx]_(i <- (A `|` B)) F i
    = op (\big[op/idx]_(i <- A) F i) (\big[op/idx]_(i <- B) F i).
Proof.
  move=> idop.
  rewrite eq_big_imfset //= big_map.
  rewrite big_undup //.
  by rewrite big_cat.
Qed.

End Misc.

From Coq Require Import Classical.

Section Comprehension.

Open Scope fset.

(* We need classical logic for this lemma *)
Lemma fset_seq_comprehension (K : choiceType) (ss : seq K) (P : K -> Prop) :
  exists C : {fset K}, forall k : K, k \in C <-> k \in ss /\ P k.
Proof.
  elim: ss.
    exists fset0 => k.
    by rewrite inE; split => [|[]].
  move=> s ss [C IHCss].
  case: (classic (P s)) => [Ps | nPs].
    exists (s |` C) => k; split.
      by move=> /[!inE] => /orP [/eqP -> /[!eqxx] //|/IHCss [-> Pk] /[!orbT]].
    move=> [/[!inE] /orP [-> //|kinss] Pk].
    apply/orP; right.
    by apply IHCss; split.
  exists C => k.
  rewrite IHCss inE; split=> -[+ Pk].
    by move=> ->; split; first by rewrite orbT.
  move=> /orP [/eqP eqks|//].
  by exfalso; apply: nPs; rewrite -eqks.
Qed.

End Comprehension.
