From mathcomp Require Import all_ssreflect finmap.

From QRC1 Require Import Preamble Language QRC1 QRC1Equiv Closure.
From QRC1 Require Import KripkeSemantics Pairs.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Section Assignify.

Open Scope qsp_scope.
Open Scope fset.

Variable sig : signature.
Variable n : nat.

Notation sign := (extend sig n).
Notation assn := (preassignment (ConstName sign)).

Definition assignify_term (exs : {fset VarName}) (g : assn) (t : term sig)
    : term sign :=
  match t with
  | Var x => if x \in exs then @Var sign x else Const (g x)
  | Const c => Const (inl c : ConstName sign)
  end.

Fixpoint assignify_exs (exs : {fset VarName}) (g : assn) (A : formula sig)
    : formula sign :=
  match A with
  | T => T sign
  | Pred P ts => @Pred sign P [tuple of map (assignify_term exs g) ts]
  | A1 /\ A2 => assignify_exs exs g A1 /\ assignify_exs exs g A2
  | <> B => <> (assignify_exs exs g B)
  | All x B => All x (assignify_exs (x |` exs) g B)
  end.

Definition assignify : assn -> formula sig -> formula sign :=
  assignify_exs fset0.

Lemma assignify_exs_fv (exs : {fset VarName}) (g : assn) (A : formula sig) :
  fv (assignify_exs exs g A) `<=` exs.
Proof.
  elim: A exs => //=.
  - move=> P ts exs.
    apply/fsubsetP => /= x /bigfcupP /= [_ /[!andbT] /mapP /= [t _ ->]].
    case: t => [/= y|//].
    by case: ifP => //= yinexs /[!inE] /eqP ->.
  - move=> A IHA B IHB exs.
    by rewrite fsubUset IHA /=.
  - move=> x A IHA exs.
    by rewrite fsubDset IHA.
Qed.

Lemma assignify_closed (g : assn) (A : formula sig) :
  closed (assignify g A).
Proof. by rewrite /closed -fsubset0 assignify_exs_fv. Qed.

Lemma assignify_exs_Xaltern (exs : {fset VarName}) (g h : assn)
    (A : formula sig) :
    Xaltern g h exs ->
  assignify_exs exs g A = assignify_exs exs h A.
Proof.
  rewrite /assignify; elim: A exs => //=.
  - move=> P ts exs exsalterngh; congr Pred.
    apply: eq_from_tnth => i /=; rewrite 2!tnth_map.
    case: (tnth ts i) => //= x.
    by case: in_fsetP => [//|/exsalterngh ->].
  - by move=> A IHA B IHB exs exsalterngh; rewrite IHA // IHB.
  - by move=> A IHA exs exsalterngh; rewrite IHA.
  - move=> x A IHA exs exsalterngh; rewrite IHA //.
    apply: XalternW exsalterngh.
    by rewrite fsubsetUr.
Qed.

Lemma assignify_eq1 (g h : assn) :
    g =1 h ->
  assignify g =1 assignify h.
Proof. by move=> A eq1gh; apply: assignify_exs_Xaltern. Qed.

End Assignify.

Section Lift_Assignify.

Variable (sig : signature).
Variables (n m : nat).

Open Scope qsp_scope.

Lemma lift_assignify_exs (exs : {fset VarName})
    (g : preassignment (ConstName (extend sig n))) (A : formula sig) :
  (assignify_exs exs g A)!!m = assignify_exs exs (inl \o g) A!!n.
Proof.
  elim: A exs => //=.
  - move=> P ts exs.
    congr Pred; apply: eq_from_tnth => i /[!tnth_map].
    case: (tnth ts i) => [x /=|//].
    by case: ifP.
  - by move=> A IHA B IHB exs; rewrite IHA IHB.
  - by move=> A IHA exs; rewrite IHA.
  - by move=> x A IHA exs; rewrite IHA.
Qed.

Lemma lift_assignify (g : preassignment (ConstName (extend sig n)))
    (A : formula sig) :
  (assignify g A)!!m = assignify (inl \o g) A!!n.
Proof. by rewrite lift_assignify_exs. Qed.

End Lift_Assignify.

Section CanonicalFrame.

Open Scope qsp_scope.
Open Scope fset.

Variable sig : signature.

Variables PP NN : formula sig.
Hypothesis closedPP : closed PP.
Hypothesis closedNN : closed NN.

Definition seedPhi : formulas sig := [fset PP; NN].

(* The prefix "c" stands for "canonical" *)
Definition cn : nat := (constantbound seedPhi).+1.

Definition csig : signature := extend sig cn.

Definition lPP := PP!!cn.
Definition lNN := NN!!cn.
Notation lseedPhi := [fset lPP; lNN].

Lemma lseedPhi_liftfs : lseedPhi = liftfs cn seedPhi.
Proof.
  apply/fsetP => /= A.
  rewrite /lseedPhi /seedPhi !inE.
  apply/orP/imfsetP => /=.
    move=> [/eqP ->|/eqP ->].
      by exists PP => //=; rewrite !inE eqxx.
    by exists NN => //=; rewrite !inE eqxx orbT.
  by move=> [_ /[!inE] /orP[/eqP ->|/eqP ->]] ->; [left|right].
Qed.

Lemma closedfs_lseedPhi : closedfs lseedPhi.
Proof.
  apply/allP => /= A.
  by rewrite !inE => /orP [/eqP ->|/eqP ->]; rewrite closed_lift.
Qed.

Definition cWType : choiceType := [choiceType of pair csig].

Definition cMType : choiceType := ConstName csig.

Definition cdomain : {fset cMType} := [fset c in cMType].

Lemma constantsfs_lseedPhi : constantsfs lseedPhi `<=` cdomain.
Proof.
  apply/fsubsetP => /= c _.
  by apply/imfsetP; exists c.
Qed.

Lemma constantbound_lseedPhi : constantbound lseedPhi < #|` cdomain|.
Proof.
  rewrite card_imfset //= -cardE card_sum card_ord /cn.
  apply: (@leq_trans (constantbound seedPhi).+1); last by apply: leq_addl.
  rewrite lseedPhi_liftfs.
  by rewrite {1}/constantbound constantcountfs_liftfs quantifierdepthfs_liftfs.
Qed.

Lemma cdomain_of_Const_subproof (c : ConstName sig) : inl c \in cdomain.
Proof. by apply/imfsetP; exists (inl c). Qed.

Definition cdomain_of_Const (c : ConstName sig) : cdomain :=
  [` cdomain_of_Const_subproof c].

Lemma cdomain_of_cConst_subproof (c : ConstName csig) : c \in cdomain.
Proof. by apply/imfsetP; exists c. Qed.

Definition cdomain_of_cConst (c : ConstName csig) : cdomain :=
  [` cdomain_of_cConst_subproof c].

(* The default constant / domain element; its definition shouldn't matter *)
Definition c0 : ConstName csig := inr ord0.
Definition d0 : cdomain := cdomain_of_cConst c0.

Definition cdomain_of_term (t : term csig) : cdomain :=
  match t with
  | Var _ => d0
  | Const c => cdomain_of_cConst c
  end.

Definition cdomains_of_terms (n : nat) (ts : n.-tuple (term csig)) :
    n.-tuple cdomain :=
  [tuple of map cdomain_of_term ts].

Definition terms_of_formula (P : PredName csig)
    (dflt : (arity P).-tuple (term csig)) (A : formula csig)
    : {fset (arity P).-tuple cdomain} :=
  match A with
  | Pred Q ts => if P == Q then
                   [fset cdomains_of_terms (insubd dflt (val ts))]
                 else fset0
  | T | _ /\ _ | <> _ | All _ _ => fset0
  end.

Definition ts0 (n : nat) : n.-tuple (term csig) := [tuple Const c0 | i < n].

Definition cassignment : Type := preassignment cdomain.

Definition cassignify_exs (exs : {fset VarName}) (g : cassignment)
    (A : formula sig) : formula csig :=
  assignify_exs exs (val \o g) A.

Definition cassignify (g : cassignment) : formula sig -> formula csig :=
  assignify (val \o g).

Lemma cassignify_exs_sub (exs : {fset VarName}) (g h : cassignment)
    (A : formula sig) (x : VarName) (c : ConstName csig) :
    x \notin exs ->
    xaltern g h x ->
    h x = cdomain_of_cConst c ->
  (cassignify_exs (x |` exs) g A)`[Var csig x <- Const c]
    = cassignify_exs exs h A.
Proof.
  move=> + xalterngh eqhx.
  elim: A exs => //=.
  - move=> P ts exs xnotinexs; congr Pred.
    apply: eq_from_tnth => /= i; rewrite !tnth_map.
    case: (tnth ts i) => [/= y|//].
    rewrite !inE.
    case: (@eqP _ y x) => /= [->|neqyx].
      rewrite eqxx.
      move: xnotinexs; case: ifP => // _ _.
      by rewrite eqhx.
    case: (_ \in _).
      by move: neqyx; case: eqP => [[->]|//].
    case: eqP => [//|_].
    congr Const; congr fsval.
    by apply: xalterngh; rewrite inE; apply/eqP.
  - move=> A IHA B IHB exs xnotinexs.
    by rewrite IHA // IHB.
  - move=> A IHA exs xnotinexs.
    by rewrite IHA.
  - move=> y A IHA exs xnotinexs.
    case: eqP => [[->]|neqVaryx].
      rewrite fsetUA fsetUid (assignify_exs_Xaltern (h := val \o h)) //.
      apply: (XalternW (X := [fset x])).
        by rewrite fsubsetUl.
      by move=> z /xalterngh /= ->.
    rewrite fsetUA [[fset y; x]]fsetUC -fsetUA IHA //.
    rewrite !inE negb_or xnotinexs andbT.
    by apply/eqP => eqxy; apply: neqVaryx; rewrite eqxy.
Qed.

Lemma cassignify_sub (g h : cassignment) (A : formula sig) (x : VarName)
    (c : ConstName csig) :
    xaltern g h x ->
    h x = cdomain_of_cConst c ->
  (cassignify_exs [fset x] g A)`[Var csig x <- Const c] = cassignify h A.
Proof.
  move=> xalterngh eqhx.
  rewrite /cassignify /assignify -/(cassignify_exs _ _ _).
  by rewrite -(@cassignify_exs_sub fset0 g _ _ x c) // fsetU0.
Qed.

Definition Phi : formulas csig := closurefs cdomain lseedPhi.

Lemma TinPhi : T csig \in Phi.
Proof.
  apply: closurefsT.
    by exists lPP; rewrite !inE eqxx.
  by exists c0; rewrite inE.
Qed.

Lemma lPPinPhi : lPP \in Phi.
Proof. by apply: closurefs_self; rewrite !inE eqxx. Qed.

Lemma lNNinPhi : lNN \in Phi.
Proof. by apply: closurefs_self; rewrite !inE eqxx orbT. Qed.

Lemma clindenbaum (P : formula csig) (ns : formulas csig) :
    P \in Phi ->
    ns `<=` Phi ->
    consistent ([fset P], ns) ->
  exists2 q : pair csig,
    subpair ([fset P], ns) q & wfpair Phi q.
Proof.
  exact: (lindenbaum constantsfs_lseedPhi closedfs_lseedPhi
                     constantbound_lseedPhi).
Qed.

(* The set of worlds will be fixed later. For now, we proceed with the canonical
model construction for a fixed domain size and set of worlds *)
Variable cworld : {fset cWType}.

Definition cR : cworld -> cworld -> bool :=
  fun w u => RR (val w) (val u).

Definition crawFrame : rawFrame cWType cMType :=
  {|
    world := cworld;
    R := cR;
    domain := fun=> cdomain;
    eta := fun=> fun=> id;
  |}.

Lemma crawFrame_adequate : adequateF crawFrame.
Proof.
  apply/adequateFP; split.
  - apply/transitiveFP => w u v.
    by apply: RR_trans.
  - by apply/transetaFP.
  - by apply/idetaFP.
Qed.

Definition cframe : frame cWType cMType := Frame crawFrame_adequate.

Definition cJ (w : cworld) (P : PredName sig)
    : {fset (arity P).-tuple cdomain} :=
  \bigcup_(A <- (val w).+) (terms_of_formula (ts0 (arity P)) A).

Definition crawModel : rawModel sig cWType cMType :=
  {|
    rawFrame_of_rawModel := crawFrame;
    I := fun=> cdomain_of_Const;
    J := cJ;
  |}.

Lemma crawModel_adequate : adequateM crawModel.
Proof.
  apply/adequateMP; split.
    by apply: crawFrame_adequate.
  by apply/concordantMP.
Qed.

Definition cmodel : model sig cWType cMType := Model crawModel_adequate.

Hypothesis cworld_wf : forall w : cmodel, wfpair Phi (val w).
Hypothesis wf_cworld : forall p : pair csig, wfpair Phi p -> p \in cworld.

Lemma world_existence (w : cworld) (P : formula csig) :
    <> P \in (val w).+ ->
  exists2 u : cworld, cR w u & P \in (val u).+.
Proof.
  move=> DPinw1.
  have [u [/wf_cworld uincworld] RRwu Pinu1] :=
    pair_existence constantsfs_lseedPhi closedfs_lseedPhi constantbound_lseedPhi
                   (cworld_wf w) DPinw1.
  by exists [` uincworld].
Qed.

Lemma Tinpos (w : cmodel) : T csig \in (val w).+.
Proof.
  move/maximalP: (wf_maximal (cworld_wf w)) => /(_ _) /(_ TinPhi) [//|].
  by move=> /(wf_consistent (cworld_wf w)).
Qed.

Lemma QRC1Proof_inpos (w : cmodel) (A : formula csig) :
    A \in Phi ->
    |- //\\ (val w).+ ~> A ->
  A \in (val w).+.
Proof.
  move=> + w1pA.
  have /maximalP maxw := wf_maximal (cworld_wf w).
  move=> /maxw [//|Ainw2]; exfalso.
  by apply: ((wf_consistent (cworld_wf w)) _ Ainw2).
Qed.

Lemma QRC1Proof_Cut_inpos (w : cmodel) (A B : formula csig) :
    B \in Phi ->
    A \in (val w).+ ->
    |- A ~> B ->
  B \in (val w).+.
Proof.
  move=> BinPhi Ainw1 ApB.
  apply: QRC1Proof_inpos => //.
  apply: (Cut _ ApB).
  by apply: bigConj_QRC1Proof.
Qed.

Lemma Conjinpos (w : cmodel) (A B : formula csig) :
    (A /\ B) \in Phi ->
  (A /\ B) \in (val w).+ = (A \in (val w).+) && (B \in (val w).+).
Proof.
  move=> /[dup] ABinPhi /[dup] /closurefsConjl AinPhi /closurefsConjr BinPhi.
  apply/idP/andP.
    by move=> /QRC1Proof_Cut_inpos inpos; split; apply: inpos.
  move=> [Ainw1 Binw1].
  apply: QRC1Proof_inpos => //.
  by apply: ConjI; apply: bigConj_QRC1Proof.
Qed.

Lemma Allinpos (w : cmodel) (x : VarName) (A : formula csig)
    (c : ConstName csig) :
  All x A \in (val w).+ -> A`[Var csig x <- Const c] \in (val w).+.
Proof.
  move=>/[dup] xAinw1 /QRC1Proof_Cut_inpos; apply.
    apply: substitution_closurefs.
      by apply/imfsetP; exists c.
    have /fsubsetP := wf_subset (cworld_wf w).
    by apply; rewrite inE xAinw1.
  by apply: All_sub; apply: freefor_Const.
Qed.

Lemma truth (w : cmodel) (g : assignment w) (A : formula sig) :
    cassignify g A \in Phi ->
  sat g A <-> cassignify g A \in (val w).+.
Proof.
  elim: A w g => /=.
  - move=> w g _; split=> // _.
    by apply: Tinpos.
  - move=> P tsP w g PinPhi.
    rewrite /cJ /cassignify /assignify /=; split.
      move=> /bigfcupP /= [A /[!andbT] Ainw1] tsinA.
      set ts := [tuple of _].
      suff <- : A = @Pred csig _ ts by [].
      case: A Ainw1 tsinA => //= Q tsQ tsQinw1.
      case: (@eqP _ P Q) => [eqPQ|//].
      move: tsQ tsQinw1; rewrite -{}eqPQ => {Q} tsQ tsQinw1.
      move=> /imfsetP /= [_ /[!inE] /eqP ->] eqt.
      rewrite {}/ts; congr Pred.
      apply: eq_from_tnth => i; rewrite tnth_map.
      move: eqt => /(congr1 (fun t => tnth t i)); rewrite !tnth_map.
      move=> /val_eqP /eqP.
      have -> : tnth (insubd (ts0 (arity P)) tsQ) i = tnth tsQ i.
        by rewrite 2!(tnth_nth (Const c0)) valKd.
      case eqtsQi : (tnth tsQ i) => [x|c] /=.
        exfalso.
        have := wf_closed (cworld_wf w).
        rewrite pair_closedfs => /andP [/allP /(_ _) /(_ tsQinw1) + _].
        move=> /(closed_fv x) /negP; apply=> /=.
        apply/bigfcupP; exists (tnth tsQ i).
          by rewrite mem_tnth.
        by rewrite eqtsQi inE.
      by case: (tnth _ i) => /= [?|?] ->;
           rewrite -eqtsQi; congr tnth; apply/val_eqP; rewrite valKd.
    set PtsP := Pred _.
    move=> Pinw1.
    apply/bigfcupP; exists PtsP => /=.
      by rewrite Pinw1.
    rewrite eqxx inE; apply/eqP.
    apply: eq_from_tnth => i; rewrite !tnth_map.
    rewrite (tnth_nth (Const c0)) valKd /= -tnth_nth tnth_map.
    by case: (tnth tsP i) => [v|c]; apply/val_eqP.
  - move=> A IHA B IHB w g.
    rewrite /cassignify /assignify /= -2!/(assignify _ _) -2!/(cassignify _ _).
    move=> /[dup] ABinPhi /[dup] /closurefsConjl AinPhi /closurefsConjr BinPhi.
    by rewrite Conjinpos // IHA // IHB //; split=> /andP.
  - move=> A IHA w g.
    rewrite /cassignify /assignify /= -/(assignify _ _) -/(cassignify _ _).
    move=> /[dup] DAinPhi /closurefsDiam AinPhi.
    split.
      move=> [u Rwu satuA].
      have: cassignify (id \o g) A \in Phi.
        by rewrite /cassignify -(assignify_eq1 (g := val \o g)).
      move=> /(IHA u) [/(_ satuA) + _].
      rewrite /cassignify -(assignify_eq1 (g := val \o g)) // => Ainu1.
      move: Rwu => /(@RR_posP _ cdomain lseedPhi _ _ (wf_subset (cworld_wf _))
                     (wf_maximal (cworld_wf _)) (wf_maximal (cworld_wf _))
                     (wf_consistent (cworld_wf _)) (wf_consistent (cworld_wf _))
                   ) [+ _]; apply=> //.
      by left.
    move=> /world_existence [u Rwu Ainu].
    exists u => //.
    by rewrite IHA.
  - move=> x A IHA w g.
    rewrite /cassignify /assignify /= fsetU0 => xAinPhi; split.
      move=> sathA.
      move/witnessedP: (wf_witnessed (cworld_wf w)) => wit.
      move/maximalP: (wf_maximal (cworld_wf w)) => /(_ _) /(_ xAinPhi) [//|].
      move=> /wit [c].
      pose h := fun y => if y == x then cdomain_of_cConst c else g y.
      have eqhxc : h x = cdomain_of_cConst c by rewrite /h eqxx.
      have xalterngh : xaltern g h x by rewrite /h => y /[!inE]; case: eqP.
      rewrite (cassignify_sub (h := h)) // => hAinw2.
      exfalso; apply: (wf_consistent (cworld_wf w) hAinw2).
      apply: bigConj_QRC1Proof.
      rewrite -IHA.
        by apply: sathA.
      rewrite -(@cassignify_sub g _ _ x c) //.
      apply: substitution_closurefs => //.
      by apply/imfsetP; exists c.
    move=> xAinw1 h xalterngh.
    have eqhx : h x = cdomain_of_cConst (val (h x)) by apply/val_eqP.
    rewrite IHA -(@cassignify_sub g _ _ x (val (h x))) //.
      by apply: Allinpos.
    apply: substitution_closurefs => //.
    by apply/imfsetP; exists (val (h x)).
Qed.

End CanonicalFrame.

Section Completeness.

Open Scope qsp_scope.
Open Scope fset.

Variable sig : signature.
Variables (AA BB : formula sig).
Hypothesis AAnpBB : ~ |- AA ~> BB.

Definition sizefv : nat := #|` fv AA `|` fv BB|.

Definition enum_fv : seq VarName := enum_fset (fv AA `|` fv BB).

Lemma enum_fvE (x : VarName) :
  (x \in enum_fv) = (x \in fv AA `|` fv BB).
Proof. by []. Qed.

Definition fv_index (x : VarName) : nat :=
  if x \in enum_fv then
    (index x enum_fv).+1
  else
    0.

Lemma fv_index_lt (x : VarName) : fv_index x < sizefv.+1.
Proof.
  rewrite /fv_index.
  case: ifP => [|//].
  by rewrite -index_mem.
Qed.

Definition closed_sig : signature := extend sig sizefv.+1.

Definition closeg (x : VarName) : ConstName closed_sig :=
  inr (Ordinal (fv_index_lt x)).

Lemma closeg_inj : {in enum_fv &, injective closeg}.
Proof.
  move=> /= x y xinfv yinfv [].
  rewrite /fv_index xinfv yinfv => - [].
  move/(congr1 (nth x enum_fv)).
  by do 2!rewrite nth_index //.
Qed.

Lemma assignify_exs_simsub (exs : {fset VarName}) (A : formula sig) :
    fv A `<=` exs `|` fv AA `|` fv BB ->
  assignify_exs exs closeg A
    = (A!!sizefv.+1)`[[seq Var closed_sig i | i <- enum_fv] <--
                      [seq if i \in exs then
                             Var closed_sig i
                           else
                             Const (closeg i)
                      | i <- enum_fv]].
Proof.
  set vars := map (Var _) _.
  pose consts (e : {fset VarName}) :=
    [seq if i \in e then Var closed_sig i else Const (closeg i) | i <- enum_fv].
  rewrite -/(consts _).
  have injVar : injective (Var closed_sig) by move=> ??[].
  elim: A exs => //=.
  - move=> P ts exs /fsubsetP fvin.
    congr Pred; apply: eq_from_tnth => i; rewrite !tnth_map /=.
    case: mapP => /=.
      move=> [x xinfv /[dup] eqlifti ->].
      rewrite (nth_map x); last by rewrite index_map // index_mem.
      rewrite index_map // nth_index //.
      by move: eqlifti; case: (tnth ts i) => [y /= [->]|//].
    case eqi : (tnth ts i) => [x /=|//].
    move=> xnotfv.
    case: ifP => [//|xnotinexs].
    exfalso; apply: xnotfv; exists x => //.
    rewrite enum_fvE.
    suff : x \in exs `|` fv AA `|` fv BB by rewrite -fsetUA inE xnotinexs /=.
    apply: fvin.
    apply/bigfcupP; exists (Var sig x).
      by rewrite -eqi mem_tnth.
    by rewrite inE.
  - by move=> A IHA B IHB exs /fsubUsetP [/IHA -> /IHB ->].
  - by move=> A IHA exs /IHA ->.
  - move=> x A IHA exs fvin.
    rewrite size_map -(size_map (Var closed_sig)) index_mem.
    rewrite -/closed_sig.
    rewrite IHA; last by move: fvin; rewrite fsubDset !fsetUA.
    case: mapP => /=.
      move=> [y + [eqxy]]; rewrite -{}eqxy => {y} xinfv.
      congr All; congr simsub.
      apply: (eq_from_nth (x0 := Var closed_sig x)).
        rewrite size_set_nth !size_map.
        apply/eqP; rewrite eq_sym; apply/eqP/maxn_idPr.
        by rewrite index_map // index_mem.
      move=> i /[!size_map] ilt.
      rewrite (nth_map x) // nth_set_nth /= index_map // !inE.
      case: eqP => /= [eqix|neqix].
        rewrite eqix -[X in index X _]eqix.
        by rewrite index_uniq ?fset_uniq // eqxx.
      case: eqP => [eqix|_].
        exfalso; apply: neqix.
        by rewrite eqix nth_index.
      by rewrite (nth_map x).
    move=> xnotfv.
    congr All; congr simsub.
    apply: (eq_from_nth (x0 := Var closed_sig x)); rewrite !size_map //.
    move=> i ilt.
    do 2!rewrite (nth_map x) //.
    rewrite !inE.
    case: eqP => [eqix|//].
    exfalso; apply: xnotfv; exists x => //.
    by rewrite -eqix mem_nth.
Qed.

Lemma assignify_simsub (A : formula sig) :
    fv A `<=` fv AA `|` fv BB ->
  assignify closeg A
    = (A!!sizefv.+1)`[[seq Var closed_sig i | i <- enum_fv] <--
                      [seq Const (closeg i) | i <- enum_fv]].
Proof. by rewrite -[_ `|` _]fset0U fsetUA => /assignify_exs_simsub. Qed.

Definition PP := assignify closeg AA.
Definition NN := assignify closeg BB.

Lemma closedPP : closed PP.
Proof. exact: assignify_closed. Qed.

Lemma closedNN : closed NN.
Proof. exact: assignify_closed. Qed.

Lemma PPnpNN : ~ |- PP ~> NN.
Proof.
  rewrite /PP /NN assignify_simsub; last by rewrite /= fsubsetUl.
  rewrite assignify_simsub; last by rewrite /= fsubsetUr.
  move=> PPpNN; apply: AAnpBB.
  apply: (QRC1Proof_lift (n := sizefv.+1)).
  apply: (ConstE_iter (xs := enum_fv) (cs := map closeg enum_fv)).
  - by apply: fset_uniq.
  - rewrite map_inj_in_uniq.
      by apply: fset_uniq.
    by apply: closeg_inj.
  - move=> _ /mapP /= [x xinenumfv ->].
    by rewrite constants_lift; apply/negP => /imfsetP [].
  - move=> _ /mapP /= [x xinenumfv ->].
    by rewrite constants_lift; apply/negP => /imfsetP [].
  by rewrite -map_comp.
Qed.

Lemma generate_worlds :
  exists cworld : {fset cWType PP NN},
    ((forall w : cmodel cworld, wfpair (Phi PP NN) (val w))
      /\ (forall p, wfpair (Phi PP NN) p -> p \in cworld))%type.
Proof.
  pose ap := allpairs Datatypes.pair (fpowerset (Phi PP NN))
                                     (fpowerset (Phi PP NN)).
  have /= [cworld wfiff] := fset_seq_comprehension ap (wfpair (Phi PP NN)).
  exists cworld; split.
    move=> w.
    have : val w \in cworld by case: w.
    by move=> /wfiff [].
  move=> p wfp.
  rewrite wfiff; split=> //.
  case: p wfp => p1 p2 wfp.
  have := wf_subset wfp.
  rewrite pair_fsubset => /= /andP [p1inPhi p2inPhi].
  by apply: allpairs_f; rewrite fpowersetE.
Qed.

Definition cg (cworld : {fset cWType PP NN}) (w : cmodel cworld)
    : assignment w :=
  insubd (d0 PP NN) \o inl \o closeg.

Lemma explicit_completeness :
  exists (cworld : {fset cWType PP NN}) (w : cmodel cworld),
    (sat (M := model_unlift (cmodel cworld)) (cg w) AA
       /\ ~ sat (M := model_unlift (cmodel cworld)) (cg w) BB)%type.
Proof.
  move: generate_worlds => [cworld [cworld_wf wf_cworld]].
  exists cworld.
  have clPP := closedPP.
  have clNN := closedNN.
  have PPnpNN := PPnpNN.
  have lNNsubPhi : [fset lNN PP NN] `<=` Phi PP NN by rewrite fsub1set lNNinPhi.
  have [] := clindenbaum clPP clNN (lPPinPhi PP NN) lNNsubPhi.
    move=> _ /[!inE] /eqP -> /=.
    by rewrite fset_seq1 bigConj1 => /QRC1Proof_lift /PPnpNN.
  move=> root /subpairP /= [subr1 subr2] wfroot.
  have rootisworld : root \in cworld by apply: wf_cworld.
  exists [` rootisworld].
  have eqgAAPP : cassignify (cg [` rootisworld]) AA!!sizefv.+1 = lPP PP NN.
    rewrite /lPP lift_assignify /cassignify.
    apply: assignify_eq1 => x /=.
    by rewrite insubdK //= !inE.
  have eqgBBNN : cassignify (cg [` rootisworld]) BB!!sizefv.+1 = lNN PP NN.
    rewrite /lNN lift_assignify /cassignify.
    apply: assignify_eq1 => x /=.
    by rewrite insubdK //= !inE.
  rewrite 2!sat_unlift.
  split.
    rewrite truth //= eqgAAPP; last by rewrite lPPinPhi.
    by apply: subr1; rewrite inE.
  rewrite truth //= eqgBBNN; last by rewrite lNNinPhi.
  apply/negP.
  rewrite (notinpos wfroot (lNNinPhi PP NN)).
  by apply: subr2; rewrite inE.
Qed.

Theorem completeness :
  exists (WType MType : choiceType) (M : model sig WType MType) (w : M)
      (g : assignment w),
    (sat g AA /\ ~ sat g BB)%type.
Proof.
  move: explicit_completeness => [cworld [w satnsat]].
  exists (cWType PP NN), (cMType PP NN), (model_unlift (cmodel cworld)).
  by exists w, (cg w).
Qed.

End Completeness.
