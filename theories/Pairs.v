From mathcomp Require Import all_ssreflect finmap.

From QRC1 Require Import Preamble Language QRC1 QRC1Equiv Closure.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Section Pairs.

Variable sig : signature.
Notation term := (term sig).
Notation ConstName := (ConstName sig).
Notation PredName := (PredName sig).
Notation formula := (formula sig).
Notation formulas := (formulas sig).
Notation consts := (consts sig).
Notation Var := (Var sig).

Open Scope qsp_scope.
Open Scope fset.

Definition pair : Type := formulas * formulas.
Notation "p .+" := (@fst formulas formulas p) (at level 2, format "p .+").
Notation "p .-" := (@snd formulas formulas p) (at level 2, format "p .-").

Coercion formulas_of_pair (p : pair) : {fset formula} := p.+ `|` p.-.

Lemma pair_fsubset (p : pair) (fs : formulas) :
  p `<=` fs = (p.+ `<=` fs) && (p.- `<=` fs).
Proof. by case: p => [p1 p2] /=; rewrite fsubUset. Qed.

Lemma p1inp (p : pair) : p.+ `<=` p.
Proof. by apply: fsubsetU; rewrite fsubset_refl. Qed.

Lemma p2inp (p : pair) : p.- `<=` p.
Proof. by apply: fsubsetU; rewrite fsubset_refl orbT. Qed.

Definition subpair (p q : pair) : bool :=
  (p.+ `<=` q.+) && (p.- `<=` q.-).
Infix "`p<=`" := subpair (at level 70).

Lemma subpairP (p q : pair) :
  reflect
    ({subset fst p <= fst q} /\ {subset snd p <= snd q})
    (p `p<=` q).
Proof. by apply: (iffP andP) => -[/fsubsetP + /fsubsetP]. Qed.

Lemma pair_closedfs (p : pair) :
  closedfs p = (closedfs p.+) && (closedfs p.-).
Proof.
  apply/allP/andP => /=.
    move=> closedp; split; apply/allP.
      by move=> A Ainp1; apply: closedp; rewrite inE Ainp1.
    by move=> A Ainp2; apply: closedp; rewrite inE Ainp2 orbT.
  move=> /= [/allP closedp1 /allP closedp2] A /fsetUP [Ainp1 | Ainp2].
    by apply: closedp1.
  by apply: closedp2.
Qed.

Definition consistent (p : pair) : Prop :=
  forall B : formula, B \in p.- -> ~ |- //\\ p.+ ~> B.

Definition maximal (fs : formulas) (p : pair) : bool :=
  [forall A : fs, (val A \in p.+) || (val A \in p.-)].

Lemma maximalP (fs : formulas) (p : pair) :
  reflect
    (forall A : formula, A \in fs -> A \in p.+ \/ A \in p.-)
    (maximal fs p).
Proof.
  apply (iffP 'forall_orP) => /=.
    move=> H A Ainfs; apply: (H [` Ainfs]).
  by move=> H A; apply: H.
Qed.

Lemma maximalE (fs : formulas) (p : pair) :
  maximal fs p = (fs `<=` p).
Proof.
  apply/maximalP/fsubsetP.
    move=> maxfs /= A Ainfs.
    by rewrite in_fsetU; apply/orP; apply: maxfs.
  move=> subfsp A Ainfs.
  have := subfsp _ Ainfs.
  by rewrite in_fsetU => /orP.
Qed.

Definition witnessed (p : pair) : bool :=
  [forall B : p.-,
    if val B is All x B' then
      [exists c : ConstName, B'`[Var x <- Const c] \in p.-]
    else true
  ].

Lemma witnessedP (p : pair) :
  reflect
    (forall (x : VarName) (A : formula), All x A \in p.- ->
      exists c : ConstName, A`[Var x <- Const c] \in p.-
    )
    (witnessed p).
Proof.
  apply: (iffP forallP) => /=.
    move=> H x A xAinp2.
    by have /= /existsP := H [` xAinp2].
  move=> H [/= [] // x A xAinp2].
  by apply/existsP; apply: H.
Qed.

Definition wfpair (fs : formulas) (p : pair) : Prop :=
  [/\ p `<=` fs, closedfs p, consistent p, maximal fs p & witnessed p].

Lemma wf_subset (fs : formulas) (p : pair) :
  wfpair fs p -> p `<=` fs.
Proof. by move=> []. Qed.

Lemma wf_closed (fs : formulas) (p : pair) :
  wfpair fs p -> closedfs p.
Proof. by move=> []. Qed.

Lemma wf_consistent (fs : formulas) (p : pair) :
  wfpair fs p -> consistent p.
Proof. by move=> []. Qed.

Lemma wf_maximal (fs : formulas) (p : pair) :
  wfpair fs p -> maximal fs p.
Proof. by move=> []. Qed.

Lemma wf_witnessed (fs : formulas) (p : pair) :
  wfpair fs p -> witnessed p.
Proof. by move=> []. Qed.

Lemma notinpos (fs : formulas) (p : pair) (A : formula) :
    wfpair fs p ->
    A \in fs ->
  (A \notin p.+) = (A \in p.-).
Proof.
  move=> wfp Ainfs.
  apply/negP/idP.
    move=> Anotinp1.
    by move/maximalP: (wf_maximal wfp) => /(_ _) /(_ Ainfs) [|].
  move=> Ainneg Ainpos.
  apply: (wf_consistent wfp); first exact: Ainneg.
  by apply: bigConj_QRC1Proof.
Qed.

Lemma counting (C1 C2 : consts) :
  #|` C1 `&` C2| < #|` C1| -> exists c, c \in C1 `\` C2.
Proof.
  move=> lt; apply/fset0Pn; apply/eqP => /eqP.
  rewrite -cardfs_eq0 cardfsD.
  rewrite subn_eq0 => le.
  by move: (leq_ltn_trans le lt); rewrite ltnn.
Qed.

Lemma exists_q (fs : formulas) (P : formula) :
  exists q : pair,
    ((forall A : formula, A \in q.+ <-> A \in fs /\ |- P ~> A)
    /\
    q.- = fs `\` q.+)%type.
Proof.
  have /= [q1 wfq1] := fset_seq_comprehension fs (fun A => |- P ~> A).
  by exists (q1, fs `\` q1).
Qed.

Definition constantbound (fs : formulas) : nat :=
  (constantcountfs fs + quantifierdepthfs fs).*2.

Lemma lindenbaum (C : consts) (fs : formulas) (P : formula) (ns : formulas) :
    constantsfs fs `<=` C ->
    closedfs fs ->
    constantbound fs < #|` C| ->
    P \in closurefs C fs ->
    ns `<=` closurefs C fs ->
    consistent ([fset P], ns) ->
  exists2 q : pair,
    ([fset P], ns) `p<=` q & wfpair (closurefs C fs) q.
Proof.
  move=> constsfsC clfs ltnC PinclCfs nsinclCfs consistPns.
  have [[q1 q2] /= [q1P q2P]] := exists_q (closurefs C fs) P.
  have Pinq1 : P \in q1 by rewrite q1P.
  exists (q1, q2).
    (* ([fset P], ns) `p<=` q *)
    apply/andP; split => /=; first by rewrite fsub1set.
    rewrite q2P.
    apply/fsubsetDP; split=> //.
    apply/fdisjointP => /= A /consistPns /=.
    rewrite enum_fsetE enum_fset1 /= bigConj1 => npPA.
    by apply/negP; rewrite q1P => -[].
  split.
  - (* q `<=` closurefs C fs *)
    rewrite pair_fsubset /=.
    rewrite q2P fsubsetDl andbT.
    apply/fsubsetP => /= A Ainq1.
    by have [/(_ Ainq1) []] := (q1P A).
  - (* closedfs q *)
    move: (closurefs_closed C clfs) => /allP /= clclfs.
    rewrite pair_closedfs /=; apply/andP; split.
      apply/allP => /= A; rewrite q1P => -[Aincl _].
      by apply: clclfs.
    apply/allP => /= A.
    by rewrite q2P => /fsetDP [Aincl _]; apply: clclfs.
  - (* consistent q *)
    move=> A /=; rewrite q2P => /fsetDP [Ainclfs /negP Anotinq1] pq1A.
    apply: Anotinq1; rewrite q1P; split=> //.
    apply: (Cut _ pq1A).
    rewrite QRC1Proof_bigConj => /= B.
    by rewrite q1P => -[].
  - (* maximal (closurefs C fs q) *)
    rewrite maximalE /formulas_of_pair /=.
    by rewrite q2P fsetUDl fsetDv fsetD0 fsubsetUr.
  - (* witnessed q *)
    apply/witnessedP => x A /=.
    rewrite q2P => /fsetDP [xAinclfs /negP xAnotinq1].
    have ltPxAC : #|` C `&` constants (P /\ All x A)| < #|` C|.
      have /fsetIidPr -> : constants (P /\ All x A) `<=` C.
        suff BC : forall B, B \in closurefs C fs -> constants B `<=` C.
          rewrite -[constants _]/(constants P `|` constants (All x A)).
          by rewrite fsubUset BC // BC.
        move=> /= B Bincl.
        move: constsfsC => /fsetUidPl <-.
        apply: (fsubset_trans _ (constantsfs_closurefs C fs)).
        by apply: in_constantsfs.
      rewrite (leq_ltn_trans _ ltnC) //.
      rewrite /= cardfsU -[constants A]/(constants (All x A)).
      rewrite (leq_trans (leq_subr _ _)) //.
      move: (constantcount_closurefs C fs) => /andP [_ leqccl].
      rewrite -![#|` constants _|]/(constantcount _) /constantbound.
      by rewrite -addnn leq_add // (leq_trans _ leqccl) // leq_bigmax_seq.
    move: (counting ltPxAC) => [c /fsetDP [cinC /=]].
    rewrite inE negb_or => /andP [cnotinP cnotinxA]; exists c.
    apply/fsetDP; split.
      by rewrite substitution_closurefs.
    apply/negP; rewrite q1P => -[Acincl pPAc].
    apply: xAnotinq1; rewrite q1P; split=> //.
    apply: (Const_AllIr _ cnotinP) => //.
    apply: closed_fv.
    by have := closurefs_closed C clfs => /allP /(_ P) /(_ PinclCfs).
Qed.

Definition RR (p q : pair) : bool :=
  [forall A in [finType of p.-],
    if val A is <> A' then [fset A'; <> A'] `<=` q.- else true
  ]
  &&
  [exists A in [finType of p.+ `&` q.-],
    if val A is <> A' then true else false
  ].

Lemma RRP (p q : pair) :
  reflect
    (
      (forall A, <> A \in p.- -> [fset A; <> A] `<=` q.-)
      /\
      (exists A, <> A \in p.+ `&` q.-)
    )
    (RR p q).
Proof.
  apply: (iffP andP).
    move=> [/forallP Hf /existsP [/= [[]] //= A Ainp1q2 _]].
    split; last by exists A.
    move=> B Binp2.
    by have /implyP /(_ isT) := Hf [` Binp2].
  move=> [Hf [A dAinp1q2]].
  split; first by apply/forallP => [[[]]].
  by apply/existsP; exists [` dAinp1q2].
Qed.

Lemma RR_posP (C : consts) (fs : formulas) (p q : pair) :
    p `<=` closurefs C fs ->
    maximal (closurefs C fs) p ->
    maximal (closurefs C fs) q ->
    consistent p ->
    consistent q ->
  reflect
    (
      (forall A,
          <> A \in (closurefs C fs) ->
          A \in q.+ \/ <> A \in q.+ ->
        <> A \in p.+
      )
      /\
      (exists A, <> A \in p.+ `&` q.-)
    )
    (RR p q).
Proof.
  move=> /fsubsetP pinfs /maximalP maxp /maximalP maxq consp consq.
  apply: (iffP (RRP _ _)).
    move=> [Hall Hex]; split=> // A /maxp [//|/Hall /fsubsetP ADAinq2].
    move=> [Ainq1|DAinq1]; exfalso.
      have /consq : A \in q.- by apply: ADAinq2; rewrite !inE eqxx.
      by apply; apply: bigConj_QRC1Proof.
    have /consq : <> A \in q.- by apply: ADAinq2; rewrite !inE eqxx orbT.
    by apply; apply: bigConj_QRC1Proof.
  move=> [Hall Hex]; split=> // A DAinp2.
  have DAinfs : <> A \in closurefs C fs.
    by apply: pinfs; move/fsubsetP: (p2inp p); apply.
  apply/fsubsetP => /= _ /[!inE] /orP [/eqP ->|/eqP ->].
    have [Ainq1|//] := maxq _ (closurefsDiam DAinfs).
    exfalso; apply: (consp _ DAinp2).
    apply: bigConj_QRC1Proof.
    by apply: Hall => //; left.
  have [DAinq1|//] := maxq _ DAinfs.
  exfalso; apply: (consp _ DAinp2).
  apply: bigConj_QRC1Proof.
  by apply: Hall => //; right.
Qed.

Lemma RR_trans : transitive RR.
Proof.
  move=> q p r /RRP /= [RRpqf [Apq /fsetIP [Apqinp1 Apqinq2]]].
  move=> /RRP /= [RRqrf [Aqr /fsetIP [Arqinq1 Arqinr2]]].
  apply/RRP; split=> /=.
    move=> A dAinp2; apply: RRqrf.
    have /fsubsetP /(_ (<> A)) := RRpqf _ dAinp2.
    by apply; rewrite !inE eqxx orbT.
  exists Apq; apply/fsetIP; split=> //.
  have /fsubsetP := RRqrf _ Apqinq2.
  by apply; rewrite !inE eqxx orbT.
Qed.

Lemma RR_irr (p : pair) : consistent p -> ~ RR p p.
Proof.
  move=> consP /RRP [_ [A /fsetIP [dAinp1 dAinp2]]].
  apply: (consP (<> A)) => //.
  by apply: bigConj_QRC1Proof.
Qed.

Lemma RR_subpair (p q r : pair) :
  RR p q -> q `p<=` r -> RR p r.
Proof.
  move=> /RRP [RRpqf [A /fsetIP [dAinp1 dAinq2]]] /andP [leqr1 leqr2].
  apply/RRP; split.
    move=> B dBinp2.
    apply: (fsubset_trans _ leqr2).
    by apply: RRpqf.
  exists A; apply/fsetIP; split=> //.
  by have /fsubsetP /(_ _) /(_ dAinq2) := leqr2.
Qed.

Definition is_diamond (A : formula) : bool :=
  if A is <> B then true else false.

Lemma is_diamondP (A : formula) :
  reflect (exists B : formula, A = <> B) (is_diamond A).
Proof.
  case: A => /= *; apply: (iffP idP) => //; try by move=> [[]].
  by eexists.
Qed.

Definition pre_diamond (A : formula) : formula :=
  if A is <> B then B else T _.

Definition diamonds_pre_diamonds (fs : formulas) : formulas :=
  [fset A in fs | is_diamond A]
    `|` [fset pre_diamond A | A in fs & is_diamond A].

Variables (C : consts) (fs : formulas).
Notation Phi := (closurefs C fs).

(* q is the successor of p for formula P *)
Definition successor (p q : pair) (P : formula) : Prop :=
  [/\ wfpair Phi q,
      RR p q &
      P \in q.+
  ].

Hypothesis constsC : constantsfs fs `<=` C.
Hypothesis clfs : closedfs fs.
Hypothesis bound : constantbound fs < #|` C|.

Lemma pair_existence (p : pair) (P : formula) :
    wfpair Phi p ->
    <> P \in p.+ ->
  exists q : pair, successor p q P.
Proof.
  move=> wfp DiamPinp1.
  set r2 : formulas := <> P |` diamonds_pre_diamonds p.-.
  (* <> P \in Phi *)
  have DiamPinPhi : <> P \in Phi.
    suff /fsubsetP /(_ (<> P)) : p.+ `<=` Phi by apply.
    apply: (fsubset_trans (p1inp _)).
    by apply: wf_subset.
  (* r2 <= Phi *)
  have r2incl : r2 `<=` Phi.
    rewrite fsubUset fsub1set DiamPinPhi /=.
    have p2incl : p.- `<=` Phi.
      apply: (fsubset_trans (p2inp _)).
      by apply: wf_subset.
    rewrite fsubUset (fsubset_trans _ p2incl) ?fset_sub //=.
    apply/fsubsetP => /= B /imfsetP /= [D].
    rewrite inE => /andP[+ /is_diamondP [E eqDDiamE] ->].
    rewrite {}eqDDiamE => DiamEinp2 /=.
    apply: closurefsDiam.
    by move: p2incl => /fsubsetP; apply.
  (* r is consistent *)
  have consr : consistent ([fset P], r2).
    move=> A /=; rewrite fset_seq1 bigConj1.
    move=> /fsetUP [/fset1P -> |].
      by apply: Diam_irreflexive.
    move=> /fsetUP [|].
      rewrite in_fset inE => /= /andP[+ /is_diamondP [B eqADiamB]].
      rewrite {}eqADiamB => DiamBinp2 pPDiamB.
      apply: (wf_consistent wfp DiamBinp2).
      have pDiamPDiamB : |- <> P ~> <> B.
        apply: (Cut _ (Trans B)).
        by apply: Nec.
      apply: (Cut _ pDiamPDiamB).
      by apply: bigConj_QRC1Proof.
    move=> /imfsetP /= [B].
    rewrite inE => /andP[+ /is_diamondP [D eqBDiamD]].
    rewrite eqBDiamD => /= Dinp2 -> pPD.
    apply: (wf_consistent wfp Dinp2).
    apply: (Cut _ (Nec pPD)).
    by apply: bigConj_QRC1Proof.
  have := lindenbaum constsC clfs bound (closurefsDiam DiamPinPhi) r2incl consr.
  move=> [q rinq wfq].
  exists q; split=> //.
    (* p RR q *)
    apply: (RR_subpair _ rinq).
    apply/RRP; split.
      move=> A DiamAinp2 /=.
      apply/fsubsetP => /= B.
      rewrite 3!inE => /orP [/eqP -> | /eqP ->].
        apply/fsetUP; right; apply/fsetUP; right.
        apply/imfsetP => /=.
        exists (<> A) => //.
        by rewrite inE; apply/andP.
      apply/fsetUP; right; apply/fsetUP; left.
      by rewrite in_fset inE; apply/andP.
    exists P.
    apply/fsetIP; split=> //=.
    by apply/fsetUP; left; rewrite inE.
  (* P in q.+ *)
  move: rinq => /andP [/fsubsetP /(_ P) /= + _].
  by rewrite inE eqxx; apply.
Qed.

End Pairs.

Notation "p .+" :=
  (@fst (formulas _) (formulas _) p) (at level 2, format "p .+").
Notation "p .-" :=
  (@snd (formulas _) (formulas _) p) (at level 2, format "p .-").
