---
fullname: Coq formalization of QRC<sub>1</sub>
shortname: QRC1-Coq
organization: ana-borges
community: false
pipeline: true
coqdoc: true
coqdoc_index: toc.html
dune: false
doi:

synopsis: >-
  Coq formalization of the quantified provability logic QRC<sub>1</sub>

description: |-
  This library defines the language, calculus and Kripke semantics of
  QRC<sub>1</sub>. There is a proof of Kripke soundness and an ongoing proof of
  completeness.

publications:
- pub_url: https://arxiv.org/pdf/2003.13651.pdf
  pub_title: Quantified Reflection Calculus with one modality
- pub_url: https://arxiv.org/pdf/2102.13091.pdf
  pub_title: An Escape from Vardanyan's Theorem
- pub_url: https://arxiv.org/pdf/2206.03358.pdf
  pub_title: Towards a Coq formalization of a quantified modal logic

authors:
- name: Ana de Almeida Borges
  initial: true

maintainers:
- name: Ana de Almeida Borges
  nickname: ana-borges

opam-file-maintainer: ana.agvb@gmail.com

opam-file-version: dev

license:
  fullname: Mozilla Public License 2.0
  identifier: MPL-2.0

supported_coq_versions:
  text: 8.16 and 8.17
  opam: '{(>= "8.16" & < "8.18~")}'

dependencies:
- opam:
    name: coq-mathcomp-ssreflect
    version: '{(>= "1.15" & < "1.18~")}'
  nix: ssreflect
  description: |-
    [MathComp](https://math-comp.github.io) 1.15 to 1.17 (`ssreflect` suffices)
- opam:
    name: coq-mathcomp-finmap
    version: '{(>= "1.5" & < "1.6~")}'
  nix: finmap
  description: |-
    [Finmap](https://github.com/math-comp/finmap) 1.5
- opam:
    name: coq-library-undecidability
    version: '{(>= "1.1+8.16" & < "1.2~")}'
  description: |-
    [Coq Library of Undecidability Proofs](https://github.com/uds-psl/coq-library-undecidability) 1.1

tested_coq_nix_versions:
- version_or_url: https://github.com/coq/coq-on-cachix/tarball/master

namespace: QRC1

keywords:
- name: provability logic
- name: strictly positive logics
- name: quantified modal logic
- name: coq formalization

categories:
- name: Logic/Modal Logic

build: |-
  ## Installation (with opam)

  1. Make sure you have Version 2 of
  [`opam`](https://opam.ocaml.org/doc/Install.html).
  
  2. Optional: create a new switch for QRC1 Coq, for example with the following
  command:
    ``` shell
    opam switch create QRC1-Coq 4.12.0
    ```
  3. Download the files:
    ``` shell
    git clone https://gitlab.com/ana-borges/QRC1-Coq.git
    cd QRC1-Coq
    ```
  4. Install:
    ``` shell
    opam install .
    ```

  ## Building for development

  1. Download the files:
    ``` shell
    git clone https://gitlab.com/ana-borges/QRC1-Coq.git
    cd QRC1-Coq
    ```
  2. Install the dependencies, for example with:
    ``` shell
    opam install . --deps-only
    ```
  3. Build:
    ``` shell
    make
    ```

documentation: |-
  ## Project summary
  
  A summary of the project linking it to the relevant references is available
  in `theories/Summary.v` and can be inspected
  [online](https://ana-borges.gitlab.io/QRC1-Coq/Summary.html).

  Locally generating the HTML version of this summary requires
  [Alectryon](https://github.com/cpitclaudel/alectryon) and can be achieved via
  `make summary`. The HTML file can then be found in
  `docs/alectryon/Summary.html`.

  ## Code Documentation

  An HTML version of the source code is available
  [online](https://ana-borges.gitlab.io/QRC1-Coq/toc.html). It is generated
  with [CoqdocJS](https://github.com/palmskog/coqdocjs).

  Locally generating the HTML version of the code can be achieved via `make
  coqdoc`. The files can then be found in `docs/coqdoc/`.

  ## File Contents

  - `Preamble.v`: facts unrelated to QRC<sub>1</sub>
  - `Language.v`: the language of QRC<sub>1</sub>
  - `QRC1.v`: the calculus QRC<sub>1</sub> and basic consequences
  - `QRC1Equiv.v`: the notion of QRC<sub>1</sub> equivalence and conjunction
    over a list of formulas
  - `KripkeSemantics.v`: the notions of frame, model, and satisfaction
  - `KripkeSoundness.v`: the soundness proof
  - `Closure.v`: the notion of closure and related results
  - `Pairs.v`: the notion of well-formed pair and related results, such as the
    Lindenbaum Lemma
  - `KripkeCompleteness.v`: the canonical model and the completeness proof
  - `Decidability.v`: the proof that QRC<sub>1</sub> is decidable
  - `Summary.v`: a summary of the main definitions and results
---
