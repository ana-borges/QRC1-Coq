all: QRC1 Summary.vo

Makefile.coq: _CoqProject
	$(COQBIN)coq_makefile -f _CoqProject -o Makefile.coq

QRC1: Makefile.coq
	@+$(MAKE) -f Makefile.coq all

Summary.vo: QRC1
	coqc \
		-w -notation-overridden \
		-w -notation-incompatible-format \
		-Q theories QRC1 \
		theories/Summary.v

summary: Summary.vo
	alectryon \
		--frontend coq+rst \
		--backend webpage \
		--output-directory docs/alectryon \
		--long-line-threshold 80 \
		--webpage-style centered \
		-Q theories QRC1 \
		theories/Summary.v

clean: Makefile.coq
	@+$(MAKE) -f Makefile.coq cleanall
	@rm -f Makefile.coq Makefile.coq.conf

clean-summary:
	@rm -rf docs/alectryon \
		theories/Summary.vo* theories/Summary.glob theories/.Summary.aux

force _CoqProject Makefile: ;

%: Makefile.coq force
	@+$(MAKE) -f Makefile.coq $@

.PHONY: all Makefile.coq QRC1 Summary.vo summary clean clean-summary force
