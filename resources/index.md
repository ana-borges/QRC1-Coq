---
# This file was generated from `meta.yml`, please do not edit manually.
# Follow the instructions on https://gitlab.com/ana-borges/templates to regenerate.
title: Coq formalization of QRC<sub>1</sub>
lang: en
header-includes:
  - |
    <style type="text/css"> body {font-family: Arial, Helvetica; margin-left: 5em; font-size: large;} </style>
    <style type="text/css"> h1 {margin-left: 0em; padding: 0px; text-align: center} </style>
    <style type="text/css"> h2 {margin-left: 0em; padding: 0px; color: #580909} </style>
    <style type="text/css"> h3 {margin-left: 1em; padding: 0px; color: #C05001;} </style>
    <style type="text/css"> body { width: 1100px; margin-left: 30px; }</style>
---

<div style="text-align:left"><img src="https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png" height="25" style="border:0px">
<a href="https://gitlab.com/ana-borges/QRC1-Coq">View the project on GitLab</a>
<img src="https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png" height="25" style="border:0px"></div>

## About

Welcome to the Coq formalization of QRC<sub>1</sub> project website!

This library defines the language, calculus and Kripke semantics of
QRC<sub>1</sub>. There is a proof of Kripke soundness and an ongoing proof of
completeness.

This is an open source project, licensed under the Mozilla Public License 2.0.

## Get the code

The current stable release of Coq formalization of QRC<sub>1</sub> can be [downloaded from GitLab](https://gitlab.com/ana-borges/QRC1-Coq/-/releases).

## Documentation

The coqdoc presentation of the source files can be browsed [here](toc.html)

Related publications, if any, are listed below.

- [Quantified Reflection Calculus with one modality](https://arxiv.org/pdf/2003.13651.pdf) 
- [An Escape from Vardanyan's Theorem](https://arxiv.org/pdf/2102.13091.pdf) 
- [Towards a Coq formalization of a quantified modal logic](https://arxiv.org/pdf/2206.03358.pdf) 

## Help and contact

- Report issues on [GitLab](https://gitlab.com/ana-borges/QRC1-Coq/issues)

## Authors and contributors

- Ana de Almeida Borges

